/********************************************************************************
 * @File name: main.c
 * @Author: Letitia-Ron/PU-TONG/ArsSama
 * @Version: 1.1
 * @Date: 2023-3-18
 * @Description: main
 ********************************************************************************/

#include "main.h"

static const char *TAG = "main";

int statu = 0;
static void code_run(void *arg)
{
    ESP_LOGW(TAG, "code start run!");
    /*debug code*/
    // generic_gpio_init();
    wifi_init();
    float i = 20.3;
    while (1)
    {
        /* code */
        ESP_LOGW(TAG, "wifi_connect status: %d", get_wifi_status()); /*获取wifi状态*/
        /*尝试重设参重连*/
        // if (statu == 8)
        // {
        //     /*wifi重设参数和重连*/
        //     wifi_reconnect(true, "Kami", "88888888");
        //     esp_wifi_connect();
        //     return ESP_OK;
        // }
        i += 0.5;
        if (i >= 100)
        {
            i = 0;
        }
        // http_post_onenet(i, i + 10);s
        vTaskDelay(2500 / portTICK_PERIOD_MS);
    }
}

static void debug_gpio(void) // arg：传递给线程主函数执行期间所使用的参数，回调函数
{
    // uint8_t pwm = 0, flag = 0;

    // 配置GPIO为输出模式
    gpio_config_t io_conf;
    io_conf.mode = GPIO_MODE_OUTPUT;
    io_conf.pull_down_en = GPIO_PULLDOWN_DISABLE;
    io_conf.pull_up_en = GPIO_PULLUP_DISABLE;
    io_conf.intr_type = GPIO_INTR_DISABLE;
    io_conf.pin_bit_mask = ((1ULL << 46) | (1ULL << 45) | (1ULL << 42) | (1ULL << 41) | (1ULL << 40) | (1ULL << 39) | (1ULL << 38) | (1ULL << 37) | (1ULL << 36) | (1ULL << 33) | (1ULL << 34) | (1ULL << 35) | (1ULL << 47) | (1ULL << 17) | (1ULL << 18) | (1ULL << 21) | (1ULL << 48));
    gpio_config(&io_conf);

    // 翻转电平
    while (1)
    {
        gpio_set_level(46, 1);
        gpio_set_level(45, 1);
        gpio_set_level(42, 1);
        gpio_set_level(41, 1);
        gpio_set_level(40, 1);
        gpio_set_level(39, 1);
        gpio_set_level(38, 1);
        gpio_set_level(37, 1);
        gpio_set_level(36, 1);
        gpio_set_level(33, 1);
        gpio_set_level(34, 1);
        gpio_set_level(35, 1);
        gpio_set_level(47, 1);
        gpio_set_level(17, 1);
        gpio_set_level(18, 1);
        gpio_set_level(21, 1);
        gpio_set_level(48, 1);
        vTaskDelay(1000 / portTICK_PERIOD_MS);
        gpio_set_level(46, 0);
        gpio_set_level(45, 0);
        gpio_set_level(42, 0);
        gpio_set_level(41, 0);
        gpio_set_level(40, 0);
        gpio_set_level(39, 0);
        gpio_set_level(38, 0);
        gpio_set_level(37, 0);
        gpio_set_level(36, 0);
        gpio_set_level(33, 0);
        gpio_set_level(34, 0);
        gpio_set_level(35, 0);
        gpio_set_level(47, 0);
        gpio_set_level(17, 0);
        gpio_set_level(18, 0);
        gpio_set_level(21, 0);
        gpio_set_level(48, 0);
        vTaskDelay(1000 / portTICK_PERIOD_MS);
    }

    // while (1)
    // {
    //     // if (flag == 0)
    //     // {
    //     //     pwm += 1;
    //     //     if (pwm >= 15)
    //     //     {
    //     //         flag = 1;
    //     //     }
    //     //     lcd_bl_set(pwm);
    //     // }
    //     // else if (flag == 1)
    //     // {
    //     //     pwm -= 1;
    //     //     if (pwm <= 0)
    //     //     {
    //     //         flag = 0;
    //     //     }
    //     //     lcd_bl_set(pwm);
    //     // }
    //     // ESP_LOGI(TAG, "bright : %d", pwm);
    //     // vTaskDelay(80 / portTICK_PERIOD_MS);
    // }
}

static void dump_haps()
{
    while (1)
    {
        // Get SRAM and PSRAM free and total size
        size_t sram_free = heap_caps_get_free_size(MALLOC_CAP_INTERNAL);
        size_t sram_total = heap_caps_get_total_size(MALLOC_CAP_INTERNAL);
        size_t psram_free = heap_caps_get_free_size(MALLOC_CAP_SPIRAM);
        size_t psram_total = heap_caps_get_total_size(MALLOC_CAP_SPIRAM);

        // Print SRAM and PSRAM information
        ESP_LOGW(TAG, "SRAM free/total: %d/%d", sram_free, sram_total);
        ESP_LOGW(TAG, "PSRAM free/total: %d/%d", psram_free, psram_total);

        vTaskDelay(pdMS_TO_TICKS(1000));
    }
}

void app_main(void)
{
    exegesis_log(); /*打印狗头[doge]*/
    create_mutex(); /*创建互斥锁*/
    // debug_gpio();
    // code_run();
    // wifi_init();
    // TaskHandle_t xTaskHandle_local_time = NULL,
    //              xTaskHandle_cpu_temperature = NULL,
    //              xTaskHandle_gui = NULL,
    //              xTaskHandle_hid = NULL;                                                             /*任务句柄的数据类型*/
    // xTaskCreatePinnedToCore(code_run, "code_run", 1024 * 6, NULL, 4, NULL, 0);
    xTaskCreate(gui_task, "gui_task", 1024 * 8, NULL, 3, NULL);           /*Little GL task*/
    xTaskCreate(hid_host_task, "hid_host_task", 1024 * 4, NULL, 4, NULL); /*hid_host task*/
    // xTaskCreate(http_post_task, "http_post_task", 1024 * 8, NULL, 5, NULL);                           /*http_post task*/
    // xTaskCreate(get_local_time_task, "get_local_time_task", 1024 * 8, NULL, 6, NULL);                 /*sntp task*/
    xTaskCreate(cpu_temperature_sensor_task, "cpu_temperature_sensor_task", 1024 * 4, NULL, 7, NULL); /*cpu_temperature_sensor_task*/
    xTaskCreate(dump_haps, "dump_haps", 1024 * 4, NULL, 8, NULL);

    // xTaskCreate(echo_task, "echo_task", ECHO_TASK_STACK_SIZE, NULL, ECHO_TASK_PRIO, NULL);             /*rs485 task*/
    // xTaskCreate(st1615_task, "st1615_task", 1024 * 4, NULL, 6, NULL);                                  /*ST1615 Task*/
    // xTaskCreate(CPU_Task, "CPU_Task", 1024 * 10, NULL, 6, NULL);                                       /*Cpu Check task*/
    // xTaskCreate(debug_gpio, "debug debug_gpio", 1024 * 4, NULL, 4, NULL);                              /*debug debug_gpio task*/

    esp_err_t err = esp_task_wdt_add(NULL);
    ESP_LOGI(TAG, "err=%d", err);
    while (1)
    {
        ESP_ERROR_CHECK(esp_task_wdt_reset());
        vTaskDelay(1000 / portTICK_PERIOD_MS);
    }
}
