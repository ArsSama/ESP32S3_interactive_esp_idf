#ifndef __MAIN_H__
#define __MAIN_H__

#include "esp_lcd_touch_st1615.h" /*st1615 touch*/
#include "lv_app_main.h"                     /*lvgl app——ui*/
#include "uart_rs485.h"                      /*RS485*/
#include "hid_host_task.h"                   /*USB HID*/
#include "wifi_init.h"                       /*wifi初始化*/
#include "http_post.h"                       /*http post*/
#include "cpu_check.h"                       /*cpu占用查询*/
#include "exegesis.h"                        /*打印狗头*/
#include "generic_gpio.h"                    /*io*/
#include "cpu_temperature_sensor.h"          /*cpu温度*/
#include "sntp_custom.h"                     /*ntp*/
#include "mutex.h"                           /*互斥锁*/
/*sd card*/

#endif
