# i80_control_idf5.0_master

#### 介绍
    本仓库在i80_control仓库基础上的组件拓展

#### 软件架构
软件架构说明

#### 实物图
1.  PCB_Layout
    ![PCB](hardware/实物图/PCB.jpg)
2.  核心驱动V1.3_B
    ![核心驱动V1.3_B](hardware/实物图/核心驱动V1.3_B.jpg)
3.  核心驱动V1.3_F
    ![核心驱动V1.3_F](hardware/实物图/核心驱动V1.3_F.jpg)
4.  核心驱动V1.3(左)V1.4(右)_B
    ![核心驱动V1.3(左)V1.4(右)_B](hardware/实物图/核心驱动V1.3(左)V1.4(右)_B.jpg)
5.  外设转接板
    ![转接板](hardware/实物图/转接板.jpg)

#### 硬件支持以及接线教程

| Supported Targets | ESP32 | ESP32-S2 | ESP32-S3 |
| ----------------- | ----- | -------- | -------- |

```
   ESP Board                      LCD Screen -->(ST7789||ST7796||ILI9488||GC9A01||NT35510(NT35510暂未测试))
┌─────────────┐              ┌────────────────┐
│             │              │                │
│         3V3 ├─────────────►│ VCC            │
│             │              │                │
│         GND ├──────────────┤ GND            │
│             │              │                │
│DATA[0-7/15] │◄────────────►│ DATA[0-7/15]   │
│             │              │                │
│        PCLK ├─────────────►│ PCLK           │
│             │              │                │
│          CS ├─────────────►│ CS             │
│             │              │                │
│         D/C ├─────────────►│ D/C            │
│             │              │                │
│         RST ├─────────────►│ RST            │
│             │              │                │
│    BK_LIGHT ├─────────────►│ BCKL           │
│             │              │                │
│             │              └────────────────┘
│             │                   LCD TOUCH -->(FT5x06||GT911||TT21100||ST1615)
│             │              ┌────────────────┐
│             │              │                │
│     I2C SCL ├─────────────►│ I2C SCL        │
│             │              │                │
│     I2C SDA │◄────────────►│ I2C SDA        │
│             │              │                │
└─────────────┘              └────────────────┘
                                   USB_HID --> USB_MOUSE
┌─────────────┐              ┌────────────────┐
│             │              │                │
│        USB  │◄────────────►│ USB_MOUSE      │
│             │              │                │
└─────────────┘              └────────────────┘



```

#### 使用说明

1.  在Configuration里面选择待驱动屏幕以及触屏。
2.  填写相应的IO引脚即可成功驱动。
3.  编译下载即可

#### 参与贡献

1.  Fork 本仓库
2.  master 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)

