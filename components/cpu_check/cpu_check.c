/********************************************************************************
 * @File name: cpu_check.c
 * @Author: Letitia-Ron/PU-TONG/ArsSama
 * @Version: 1.0
 * @Date: 2023-4-15
 * @Description: cpu_check
 ********************************************************************************/

#include <stdio.h>
#include "cpu_check.h"

/*
vTaskGetRunTimeStats() 使用
注意:
使用 vTaskGetRunTimeStats() 前需使能:
make menuconfig -> Component config -> FreeRTOS -> Enable FreeRTOS trace facility
make menuconfig -> Component config -> FreeRTOS -> Enable FreeRTOS trace facility -> Enable FreeRTOS stats formatting functions
make menuconfig -> Component config -> FreeRTOS -> Enable FreeRTOS to collect run time stats
通过上面配置，等同于使能 FreeRTOSConfig.h 中如下三个宏:
configGENERATE_RUN_TIME_STATS，configUSE_STATS_FORMATTING_FUNCTIONS 和 configSUPPORT_DYNAMIC_ALLOCATION
*/


void CPU_Task(void *arg)
{
    uint8_t CPU_RunInfo[400]; // 保存任务运行时间信息
    while (1)
    {
        memset(CPU_RunInfo, 0, 400); /* 信息缓冲区清零 */

        vTaskList((char *)&CPU_RunInfo); // 获取任务运行时间信息

        printf("----------------------------------------------------\r\n");
        printf("task_name     task_status     priority stack task_id\r\n");
        printf("%s", CPU_RunInfo);
        printf("----------------------------------------------------\r\n");

        memset(CPU_RunInfo, 0, 400); /* 信息缓冲区清零 */

        vTaskGetRunTimeStats((char *)&CPU_RunInfo);

        printf("task_name      run_cnt                 usage_rate   \r\n");
        printf("%s", CPU_RunInfo);
        printf("----------------------------------------------------\r\n");

        vTaskDelay(500 / portTICK_PERIOD_MS);
    }
}