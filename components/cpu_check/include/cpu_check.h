#ifndef __CPU_CHECK_H__
#define __CPU_CHECK_H__

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_timer.h"
#include "string.h"

void CPU_Task(void *arg);


#endif