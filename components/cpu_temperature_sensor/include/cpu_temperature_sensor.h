#ifndef __CPU_TEMPERATURE_SENSOR_H
#define __CPU_TEMPERATURE_SENSOR_H

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_task_wdt.h"
#include "esp_log.h"
#include "driver/temperature_sensor.h"
/*mutex*/
#include "mutex.h"


#define ESP_LOG 1

extern float cpu_tsens_value;


void cpu_temperature_sensor_task(void *arg);

#endif