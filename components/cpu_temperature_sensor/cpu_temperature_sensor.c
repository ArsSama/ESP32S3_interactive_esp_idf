/********************************************************************************
 * @File name: cpu_temperature_sensor.c
 * @Author: Letitia-Ron/PU-TONG/ArsSama
 * @Version: 1.0
 * @Date: 2023-4-15
 * @Description: cpu_temperature_sensor
 ********************************************************************************/

#include "cpu_temperature_sensor.h"

static const char *TAG = "cpu_temperature_sensor";

float cpu_tsens_value;

void cpu_temperature_sensor_task(void *arg)
{
    // ESP_ERROR_CHECK(esp_task_wdt_add(NULL)); // 为当前任务启用看门狗定时器
    TickType_t xLastWakeTime = xTaskGetTickCount(); /*第一次运行时获取当前系统时间*/

    ESP_LOGI(TAG, "Install temperature sensor, expected temp ranger range: 10~50 ℃");
    temperature_sensor_handle_t temp_sensor = NULL;
    temperature_sensor_config_t temp_sensor_config = TEMPERATURE_SENSOR_CONFIG_DEFAULT(10, 50);
    ESP_ERROR_CHECK(temperature_sensor_install(&temp_sensor_config, &temp_sensor));

    ESP_LOGI(TAG, "Enable temperature sensor");
    ESP_ERROR_CHECK(temperature_sensor_enable(temp_sensor));

    ESP_LOGI(TAG, "Read temperature");
    // 配置看门狗
    esp_task_wdt_add(NULL);
    while (1)
    {
        xSemaphoreTake(cpu_temp_mutex, portMAX_DELAY); // 获取CPU温度的互斥锁
        /*访问共享资源-CPU温度*/
        ESP_ERROR_CHECK(temperature_sensor_get_celsius(temp_sensor, &cpu_tsens_value));
#if ESP_LOG
        ESP_LOGI(TAG, "Temperature value %.02f ℃", cpu_tsens_value);
#endif
        xSemaphoreGive(cpu_temp_mutex); // 释放互斥锁

        // ESP_ERROR_CHECK(esp_task_wdt_reset()); // 重置任务看门狗定时器
        esp_task_wdt_reset();                                 // 喂狗，以防止看门狗因为系统无响应而触发重启操作
        vTaskDelayUntil(&xLastWakeTime, pdMS_TO_TICKS(2000)); /*每次运行时延迟 1000ms，以确保任务能够及时释放 CPU 资源*/
        // vTaskDelay(pdMS_TO_TICKS(5000));
        // vTaskDelay(3000 / portTICK_PERIOD_MS);
    }
}
