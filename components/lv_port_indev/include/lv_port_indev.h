#ifndef __LV_PORT_INDEV_H
#define __LV_PORT_INDEV_H

#include "lvgl.h"
#include <esp_log.h>

#if CONFIG_BSP_LCD_TOUCH_ENABLED
#include "driver/i2c.h"
#if CONFIG_BSP_LCD_TOUCH_CONTROLLER_GT911
#include "esp_lcd_touch_gt911.h"
#elif CONFIG_BSP_LCD_TOUCH_CONTROLLER_TT21100
#include "esp_lcd_touch_tt21100.h"
#elif CONFIG_BSP_LCD_TOUCH_CONTROLLER_FT5X06
#include "esp_lcd_touch_ft5x06.h"
#endif
#endif

#ifdef CONFIG_BSP_LCD_TOUCH_ENABLED

#if CONFIG_LV_TOUCH_I2C_PORT_0
#define BSP_I2C_NUM 0
#else
#define BSP_I2C_NUM 1
#endif

#define BSP_I2C_SCL CONFIG_LV_TOUCH_I2C_SCL
#define BSP_I2C_SDA CONFIG_LV_TOUCH_I2C_SDA


#if CONFIG_LV_TOUCH_SWAPXY
#define LV_TOUCH_SWAPXY 1
#else
#define LV_TOUCH_SWAPXY 0
#endif

#if CONFIG_LV_TOUCH_INVERT_X
#define LV_TOUCH_INVERT_X 1
#else
#define LV_TOUCH_INVERT_X 0
#endif

#if CONFIG_LV_TOUCH_INVERT_Y
#define LV_TOUCH_INVERT_Y 1
#else
#define LV_TOUCH_INVERT_Y 0
#endif


#endif

#if CONFIG_BSP_LCD_TOUCH_ENABLED
void BSP_lvgl_touch_cb(lv_indev_drv_t *drv, lv_indev_data_t *data);/*触屏数据回调*/
#endif


#if CONFIG_BSP_MOUSE_ENABLED
void lv_port_indev_init(void);/*鼠标接入lvgl*/
#endif

#endif
