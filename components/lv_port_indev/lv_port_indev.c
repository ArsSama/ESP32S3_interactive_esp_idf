/********************************************************************************
 * @File name: lv_port_indev.c
 * @Author: Letitia-Ron/PU-TONG/ArsSama
 * @Version: 1.0
 * @Date: 2023-4-15
 * @Description: lv_port_indev
 ********************************************************************************/

#include <stdio.h>
#include "lv_port_indev.h"
#include "hid_host_task.h"
#include "esp_lcd_touch.h"
#include "esp_lcd_touch_st1615.h"

static const char *TAG = "lv_port_indev";

#if CONFIG_BSP_LCD_TOUCH_ENABLED
void BSP_lvgl_touch_cb(lv_indev_drv_t *drv, lv_indev_data_t *data)
{

/* Read touch controller data */
#if !CONFIG_BSP_LCD_TOUCH_CONTROLLER_ST1615
    esp_lcd_touch_read_data(drv->user_data);
#endif

/* Get coordinates */
#if CONFIG_BSP_LCD_TOUCH_CONTROLLER_ST1615
    uint16_t x, y;
    bool touchpad_pressed = !(gpio_get_level(18));
    st1615_read_pos(&x, &y);
#else
    uint16_t touchpad_x[1] = {0};
    uint16_t touchpad_y[1] = {0};
    uint8_t touchpad_cnt = 0;
    bool touchpad_pressed = esp_lcd_touch_get_coordinates(drv->user_data, touchpad_x, touchpad_y, NULL, &touchpad_cnt, 1);
#endif

/* Copy coordinates */
#if CONFIG_BSP_LCD_TOUCH_CONTROLLER_ST1615
    if (touchpad_pressed > 0)
    {
        data->point.x = x;
        data->point.y = y;
        data->state = LV_INDEV_STATE_PRESSED;
        //ESP_LOGI(TAG, "x->%d  y->%d", data->point.x, data->point.y);
    }
#else
    if (touchpad_pressed && touchpad_cnt > 0)
    {
        data->point.x = touchpad_x[0];
        data->point.y = touchpad_y[0];
        data->state = LV_INDEV_STATE_PRESSED;
        // ESP_LOGI(TAG, "x->%d  y->%d", data->point.x, data->point.y);
    }
#endif
    else
    {
        data->state = LV_INDEV_STATE_RELEASED;
    }
}
#endif

#if CONFIG_BSP_MOUSE_ENABLED

static bool mouse_is_pressed(void);
static void mouse_read(lv_indev_drv_t *indev_drv, lv_indev_data_t *data);
static void mouse_get_xy(lv_coord_t *x, lv_coord_t *y);

lv_indev_t *indev_mouse;

void lv_port_indev_init(void)
{
    static lv_indev_drv_t indev_drv;

    /*Initialize your mouse if you have*/
    // mouse_init();

    /*Register a mouse input device*/
    lv_indev_drv_init(&indev_drv);
    indev_drv.type = LV_INDEV_TYPE_POINTER;
    indev_drv.read_cb = mouse_read;
    indev_mouse = lv_indev_drv_register(&indev_drv);

    /*Set cursor. For simplicity set a HOME symbol now.*/
    LV_IMG_DECLARE(ui_img_mouse_icon_blue_png); /*Declare the image source.*/
    lv_obj_t *mouse_cursor = lv_img_create(lv_scr_act());
    lv_img_set_src(mouse_cursor, &ui_img_mouse_icon_blue_png);
    lv_indev_set_cursor(indev_mouse, mouse_cursor);

    // LV_IMG_DECLARE(ui_img_mouse_icon_png);              /*Declare the image source.*/
    // lv_obj_t *cursor_obj = lv_img_create(lv_scr_act()); /*Create an image object for the cursor */
    // lv_img_set_src(cursor_obj, &ui_img_mouse_icon_png); /*Set the image source*/
    // lv_indev_set_cursor(mouse_indev, cursor_obj);       /*Connect the image  object to the driver*/
}

/*Will be called by the library to read the mouse*/
static void mouse_read(lv_indev_drv_t *indev_drv, lv_indev_data_t *data)
{
    /*Get the current x and y coordinates*/
    mouse_get_xy(&data->point.x, &data->point.y);

    /*Get whether the mouse button is pressed or released*/
    if (mouse_is_pressed())
    {
        data->state = LV_INDEV_STATE_PR;
    }
    else
    {
        data->state = LV_INDEV_STATE_REL;
    }
}

/*Return true is the mouse button is pressed*/
static bool mouse_is_pressed(void)
{
    /*Your code comes here*/
    if (mouse_data.button1)
    {
        return true;
    }

    return false;
}

/*Get the x and y coordinates if the mouse is pressed*/
static void mouse_get_xy(lv_coord_t *x, lv_coord_t *y)
{
    /*Your code comes here*/
    (*x) = mouse_data.x;
    (*y) = mouse_data.y;
}

#endif
