#ifndef __MUTEX_H
#define __MUTEX_H

#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/semphr.h"

extern SemaphoreHandle_t local_time_mutex; /*本地时间资源互斥锁*/
extern SemaphoreHandle_t cpu_temp_mutex; /*cpu温度资源互斥锁*/

void create_mutex(void);

#endif