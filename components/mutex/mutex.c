/********************************************************************************
 * @File name: mutex.c
 * @Author: Letitia-Ron/PU-TONG/ArsSama
 * @Version: 1.0
 * @Date: 2023-4-15
 * @Description: mutex
 ********************************************************************************/

#include "mutex.h"

// 定义互斥锁
SemaphoreHandle_t local_time_mutex; /*本地时间资源的互斥锁*/
SemaphoreHandle_t cpu_temp_mutex;   /*cpu温度资源互斥锁*/

/**
 * @brief create_mutex 创建互斥锁
 *
 */
void create_mutex(void)
{
    local_time_mutex = xSemaphoreCreateMutex(); /*创建本地时间资源的互斥锁，防止任务间资源冲突*/
    cpu_temp_mutex = xSemaphoreCreateMutex();   /*创建cpu温度资源的互斥锁，防止任务间资源冲突*/
}
