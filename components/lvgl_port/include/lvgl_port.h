#ifndef __LV_PORT_DISP_H
#define __LV_PORT_DISP_H

#include <esp_log.h>
#include <esp_heap_caps.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <freertos/semphr.h>
#include <driver/gpio.h>
#include <sdkconfig.h>

#include "i80.h"
#include "lvgl.h"
#include "demos/lv_demos.h"

#if CONFIG_BSP_LCD_I80_COLOR_IN_PSRAM
#define PSRAM_DATA_ALIGNMENT   64   /* Supported alignment: 16, 32, 64. A higher alignment can enables higher burst transfer size, thus a higher i80 bus throughput. */
#define LVGL_BUFF_SIZE (CONFIG_LCD_HOR_RES * 30)
#else
#define LVGL_BUFF_SIZE (CONFIG_LCD_HOR_RES * 30)
#endif

void lv_port_disp_init(void);

#endif
