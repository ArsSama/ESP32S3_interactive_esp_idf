/********************************************************************************
 * @File name: lvgl_port.c
 * @Author: Letitia-Ron/PU-TONG/ArsSama
 * @Version: 1.1
 * @Date: 2023-3-10
 * @Description: lvgl设备输入接口
 ********************************************************************************/

#include <stdio.h>
#include "lvgl_port.h"
#include "lv_port_indev.h"
#include "esp_lcd_touch.h"
#include "esp_lcd_touch_st1615.h"

static const char *TAG = "lv_port_disp";

/**
 * @brief 传输
 *
 * @param disp_drv HAL注册的驱动程序结构
 * @param area 屏幕的一个区域。
 * @param color_p
 */
static void disp_flush(lv_disp_drv_t *disp_drv, const lv_area_t *area, lv_color_t *color_p)
{
    /* 启动新的传输 */
    lcd_draw_rect(disp_drv->user_data, area->x1, area->y1, area->x2, area->y2, color_p);
    /* 通知lvgl传输已完成 */
    lv_disp_flush_ready(disp_drv);
}

/**
 * @brief 屏幕触屏接口初始化
 *
 */
void lv_port_disp_init(void)
{
    /* 申请lvgl渲染缓冲区 */
    lv_color_t *lvgl_draw_buff1 = NULL;
    lv_color_t *lvgl_draw_buff2 = NULL;
#if CONFIG_BSP_LCD_I80_COLOR_IN_PSRAM
    lvgl_draw_buff1 = heap_caps_aligned_alloc(PSRAM_DATA_ALIGNMENT, LVGL_BUFF_SIZE * sizeof(lv_color_t), MALLOC_CAP_SPIRAM | MALLOC_CAP_8BIT);
    assert(lvgl_draw_buff1);
    lvgl_draw_buff2 = heap_caps_aligned_alloc(PSRAM_DATA_ALIGNMENT, LVGL_BUFF_SIZE * sizeof(lv_color_t), MALLOC_CAP_SPIRAM | MALLOC_CAP_8BIT);
    assert(lvgl_draw_buff2);
#else
    lvgl_draw_buff1 = heap_caps_malloc(LVGL_BUFF_SIZE * sizeof(lv_color_t), MALLOC_CAP_DMA | MALLOC_CAP_INTERNAL);
    assert(lvgl_draw_buff1);
    lvgl_draw_buff2 = heap_caps_malloc(LVGL_BUFF_SIZE * sizeof(lv_color_t), MALLOC_CAP_DMA | MALLOC_CAP_INTERNAL);
    assert(lvgl_draw_buff2);
#endif
    ESP_LOGI(TAG, "buf1@%p, buf2@%p", lvgl_draw_buff1, lvgl_draw_buff2);

    /* 向lvgl注册缓冲区 */
    static lv_disp_draw_buf_t draw_buf_dsc; // 需要全程生命周期，设置为静态变量
#if CONFIG_BSP_LCD_I80_COLOR_IN_PSRAM
    lv_disp_draw_buf_init(&draw_buf_dsc, lvgl_draw_buff1, lvgl_draw_buff2, LVGL_BUFF_SIZE);
#else
    lv_disp_draw_buf_init(&draw_buf_dsc, lvgl_draw_buff1, lvgl_draw_buff2, LVGL_BUFF_SIZE);
#endif

    /* 创建并初始化用于在lvgl中注册显示设备的结构 */
    static lv_disp_drv_t disp_drv;
    lv_disp_drv_init(&disp_drv); // 使用默认值初始化该结构
    /* 设置屏幕分辨率 */
    disp_drv.hor_res = CONFIG_LCD_HOR_RES;
    disp_drv.ver_res = CONFIG_LCD_VER_RES;
    /* 初始化LCD总线 */
    static esp_lcd_panel_io_handle_t panel_io; // 需要全程生命周期，设置为静态变量
#if CONFIG_BSP_LCD_I80_COLOR_IN_PSRAM
    panel_io = lcd_i80_bus_io_init(CONFIG_LVGL_LCD_PCLK_FREQ / 3, LVGL_BUFF_SIZE * sizeof(lv_color_t)); // 初始化8080并行总线
#else
    panel_io = lcd_i80_bus_io_init(CONFIG_LVGL_LCD_PCLK_FREQ, LVGL_BUFF_SIZE * sizeof(lv_color_t)); // 初始化8080并行总线
#endif
    /* 将总线句柄放入lv_disp_drv_t中用户自定义段 */
    disp_drv.user_data = panel_io;

// 使能屏幕RD
#if (CONFIG_BSP_LCD_RD_PIN > 0)
    gpio_set_direction(CONFIG_BSP_LCD_RD_PIN, GPIO_MODE_OUTPUT);
    gpio_set_level(CONFIG_BSP_LCD_RD_PIN, 1); // 一定要拉高
    ESP_LOGI(TAG, "RD Init ");
#endif

    Lcd_reset(panel_io); // LCD复位

/* 初始化屏幕寄存器 */
#if defined(CONFIG_LVGL_LCD_PANEL_W350CE024A_40Z)
    lcd_init_reg(panel_io, panel_st7796s_w350ce024a_40z_reg_table); /*暂不用*/
#elif defined(CONFIG_BSP_LCD_I80_CONTROLLER_ST7796)
    lcd_init_reg(panel_io, panel_st7796s_w350ce024a_40z_reg_table);
#elif defined(CONFIG_BSP_LCD_PANEL_CL35BC106_40A)
    lcd_init_reg(panel_io, panel_ili9488_cl35bc106_40a_reg_table); /*暂不用*/
#elif defined(CONFIG_BSP_LCD_I80_CONTROLLER_ILI9488)
    lcd_init_reg(panel_io, panel_ili9488_P0395H01_reg_table);
#elif defined(CONFIG_BSP_LCD_I80_CONTROLLER_NT35510)
    lcd_init_reg(panel_io, panel_NT35510_reg_table);
#elif defined(CONFIG_BSP_LCD_I80_CONTROLLER_GC9A01)
    lcd_init_reg(panel_io, panel_GC9A01_reg_table);
#endif

#if CONFIG_BSP_LCD_I80_COLOR_IN_PSRAM
    ESP_LOGI(TAG, "lcd clock: %dMHz, mininal fps: %d", CONFIG_LVGL_LCD_PCLK_FREQ / 3,
             CONFIG_LVGL_LCD_PCLK_FREQ / 3 * 1000000 / (LVGL_BUFF_SIZE));
#else
    ESP_LOGI(TAG, "lcd clock: %dMHz, mininal fps: %d", CONFIG_LVGL_LCD_PCLK_FREQ,
             CONFIG_LVGL_LCD_PCLK_FREQ * 1000000 / (LVGL_BUFF_SIZE));
#endif

    /* 设置显示矩形函数，用于将矩形缓冲区刷新到屏幕上 */
    disp_drv.flush_cb = disp_flush;
    /* 设置缓冲区 */
    disp_drv.draw_buf = &draw_buf_dsc;
    /* 注册显示设备 */
    lv_disp_drv_register(&disp_drv);
    /* 开启显示（背光初始化） */
    lcd_disp_switch(panel_io, true, 100);

#if CONFIG_BSP_MOUSE_ENABLED
    /*鼠标初始化接入*/
    lv_port_indev_init();
#endif

    /*触屏初始化以及接入*/
#if CONFIG_BSP_LCD_TOUCH_ENABLED
    esp_lcd_touch_handle_t tp = NULL;
    esp_lcd_panel_io_handle_t tp_io_handle = NULL;

    ESP_LOGI(TAG, "Initialize I2C");

    const i2c_config_t i2c_conf = {
        .mode = I2C_MODE_MASTER,
        .sda_io_num = BSP_I2C_SDA,
        .scl_io_num = BSP_I2C_SCL,
        .sda_pullup_en = GPIO_PULLUP_ENABLE,
        .scl_pullup_en = GPIO_PULLUP_ENABLE,
        .master.clk_speed = 400000,
    };
    /* Initialize I2C */
    ESP_ERROR_CHECK(i2c_param_config(BSP_I2C_NUM, &i2c_conf));
    ESP_ERROR_CHECK(i2c_driver_install(BSP_I2C_NUM, i2c_conf.mode, 0, 0, 0));

#if CONFIG_BSP_LCD_TOUCH_CONTROLLER_GT911
    esp_lcd_panel_io_i2c_config_t tp_io_config = ESP_LCD_TOUCH_IO_I2C_GT911_CONFIG();
#elif CONFIG_BSP_LCD_TOUCH_CONTROLLER_TT21100
    esp_lcd_panel_io_i2c_config_t tp_io_config = ESP_LCD_TOUCH_IO_I2C_TT21100_CONFIG();
#elif CONFIG_BSP_LCD_TOUCH_CONTROLLER_FT5X06
    esp_lcd_panel_io_i2c_config_t tp_io_config = ESP_LCD_TOUCH_IO_I2C_FT5x06_CONFIG();
#elif CONFIG_BSP_LCD_TOUCH_CONTROLLER_ST1615
    esp_lcd_panel_io_i2c_config_t tp_io_config = ESP_LCD_TOUCH_IO_I2C_ST1615_CONFIG();
#endif

    ESP_LOGI(TAG, "Initialize touch IO (I2C)");

    /* Touch IO handle */
    ESP_ERROR_CHECK(esp_lcd_new_panel_io_i2c((esp_lcd_i2c_bus_handle_t)BSP_I2C_NUM, &tp_io_config, &tp_io_handle));

    esp_lcd_touch_config_t tp_cfg = {
        .x_max = CONFIG_LCD_VER_RES,
        .y_max = CONFIG_LCD_HOR_RES,
        .rst_gpio_num = -1, /*暂不设置*/
        .int_gpio_num = -1, /*暂不设置*/
        .flags = {
            .swap_xy = LV_TOUCH_SWAPXY,
            .mirror_x = LV_TOUCH_INVERT_X,
            .mirror_y = LV_TOUCH_INVERT_Y,
        },
    };
/* Initialize touch */
#if CONFIG_BSP_LCD_TOUCH_CONTROLLER_GT911
    ESP_LOGI(TAG, "Initialize touch controller GT911");
    ESP_ERROR_CHECK(esp_lcd_touch_new_i2c_gt911(tp_io_handle, &tp_cfg, &tp));
#elif CONFIG_BSP_LCD_TOUCH_CONTROLLER_TT21100
    ESP_LOGI(TAG, "Initialize touch controller TT21100");
    ESP_ERROR_CHECK(esp_lcd_touch_new_i2c_tt21100(tp_io_handle, &tp_cfg, &tp));
#elif CONFIG_BSP_LCD_TOUCH_CONTROLLER_FT5X06
    ESP_LOGI(TAG, "Initialize touch controller FT5X06");
    ESP_ERROR_CHECK(esp_lcd_touch_new_i2c_ft5x06(tp_io_handle, &tp_cfg, &tp));
#elif CONFIG_BSP_LCD_TOUCH_CONTROLLER_ST1615
    ESP_LOGI(TAG, "Initialize touch controller ST1615");
    ESP_ERROR_CHECK(esp_lcd_touch_new_i2c_st1615(/*tp_io_handle, &tp_cfg, &tp*/));
#endif

    /*注册触屏*/
    static lv_indev_drv_t indev_drv; // Input device driver (Touch)
    lv_indev_drv_init(&indev_drv);
    indev_drv.type = LV_INDEV_TYPE_POINTER;
    indev_drv.read_cb = BSP_lvgl_touch_cb;
    indev_drv.user_data = tp;

    lv_indev_drv_register(&indev_drv);
#endif
}
