/********************************************************************************
 * @File name: exegesis.c
 * @Author: Letitia-Ron/PU-TONG/ArsSama
 * @Version: 1.0
 * @Date: 2023-4-15
 * @Description: exegesis
 ********************************************************************************/

#include "exegesis.h"

static const char *TAG = "exegesis";

/***
 *          .,:,,,                                        .::,,,::.
 *        .::::,,;;,                                  .,;;:,,....:i:
 *        :i,.::::,;i:.      ....,,:::::::::,....   .;i:,.  ......;i.
 *        :;..:::;::::i;,,:::;:,,,,,,,,,,..,.,,:::iri:. .,:irsr:,.;i.
 *        ;;..,::::;;;;ri,,,.                    ..,,:;s1s1ssrr;,.;r,
 *        :;. ,::;ii;:,     . ...................     .;iirri;;;,,;i,
 *        ,i. .;ri:.   ... ............................  .,,:;:,,,;i:
 *        :s,.;r:... ....................................... .::;::s;
 *        ,1r::. .............,,,.,,:,,........................,;iir;
 *        ,s;...........     ..::.,;:,,.          ...............,;1s
 *       :i,..,.              .,:,,::,.          .......... .......;1,
 *      ir,....:rrssr;:,       ,,.,::.     .r5S9989398G95hr;. ....,.:s,
 *     ;r,..,s9855513XHAG3i   .,,,,,,,.  ,S931,.,,.;s;s&BHHA8s.,..,..:r:
 *    :r;..rGGh,  :SAG;;G@BS:.,,,,,,,,,.r83:      hHH1sXMBHHHM3..,,,,.ir.
 *   ,si,.1GS,   sBMAAX&MBMB5,,,,,,:,,.:&8       3@HXHBMBHBBH#X,.,,,,,,rr
 *   ;1:,,SH:   .A@&&B#&8H#BS,,,,,,,,,.,5XS,     3@MHABM&59M#As..,,,,:,is,
 *  .rr,,,;9&1   hBHHBB&8AMGr,,,,,,,,,,,:h&&9s;   r9&BMHBHMB9:  . .,,,,;ri.
 *  :1:....:5&XSi;r8BMBHHA9r:,......,,,,:ii19GG88899XHHH&GSr.      ...,:rs.
 *  ;s.     .:sS8G8GG889hi.        ....,,:;:,.:irssrriii:,.        ...,,i1,
 *  ;1,         ..,....,,isssi;,        .,,.                      ....,.i1,
 *  ;h:               i9HHBMBBHAX9:         .                     ...,,,rs,
 *  ,1i..            :A#MBBBBMHB##s                             ....,,,;si.
 *  .r1,..        ,..;3BMBBBHBB#Bh.     ..                    ....,,,,,i1;
 *   :h;..       .,..;,1XBMMMMBXs,.,, .. :: ,.               ....,,,,,,ss.
 *    ih: ..    .;;;, ;;:s58A3i,..    ,. ,.:,,.             ...,,,,,:,s1,
 *    .s1,....   .,;sh,  ,iSAXs;.    ,.  ,,.i85            ...,,,,,,:i1;
 *     .rh: ...     rXG9XBBM#M#MHAX3hss13&&HHXr         .....,,,,,,,ih;
 *      .s5: .....    i598X&&A&AAAAAA&XG851r:       ........,,,,:,,sh;
 *      . ihr, ...  .         ..                    ........,,,,,;11:.
 *         ,s1i. ...  ..,,,..,,,.,,.,,.,..       ........,,.,,.;s5i.
 *          .:s1r,......................       ..............;shs,
 *          . .:shr:.  ....                 ..............,ishs.
 *              .,issr;,... ...........................,is1s;.
 *                 .,is1si;:,....................,:;ir1sr;,
 *                    ..:isssssrrii;::::::;;iirsssssr;:..
 *                         .,::iiirsssssssssrri;;:.
 */

/**
 * ESP_LOGW -> yellow
 * ESP_LOGI -> green
 * ESP_LOGE -> red
 * 有点意思，hhhh
 */
#define color_log ESP_LOGW

void exegesis_log(void)
{
    color_log(TAG, "");
    color_log(TAG, "---------------------------------------------------------------------------");
    color_log(TAG, "          .,:,,,                                        .::,,,::.          ");
    color_log(TAG, "        .::::,,;;,                                  .,;;:,,....:i:         ");
    color_log(TAG, "        :i,.::::,;i:.      ....,,:::::::::,....   .;i:,.  ......;i.        ");
    color_log(TAG, "        :;..:::;::::i;,,:::;:,,,,,,,,,,..,.,,:::iri:. .,:irsr:,.;i.        ");
    color_log(TAG, "        ;;..,::::;;;;ri,,,.                    ..,,:;s1s1ssrr;,.;r,        ");
    color_log(TAG, "        :;. ,::;ii;:,     . ...................     .;iirri;;;,,;i,        ");
    color_log(TAG, "        ,i. .;ri:.   ... ............................  .,,:;:,,,;i:        ");
    color_log(TAG, "        :s,.;r:... ....................................... .::;::s;        ");
    color_log(TAG, "        ,1r::. .............,,,.,,:,,........................,;iir;        ");
    color_log(TAG, "        ,s;...........     ..::.,;:,,.          ...............,;1s        ");
    color_log(TAG, "       :i,..,.              .,:,,::,.          .......... .......;1,       ");
    color_log(TAG, "      ir,....:rrssr;:,       ,,.,::.     .r5S9989398G95hr;. ....,.:s,      ");
    color_log(TAG, "     ;r,..,s9855513XHAG3i   .,,,,,,,.  ,S931,.,,.;s;s&BHHA8s.,..,..:r:     ");
    color_log(TAG, "    :r;..rGGh,  :SAG;;G@BS:.,,,,,,,,,.r83:      hHH1sXMBHHHM3..,,,,.ir.    ");
    color_log(TAG, "   ,si,.1GS,   sBMAAX&MBMB5,,,,,,:,,.:&8       3@HXHBMBHBBH#X,.,,,,,,rr    ");
    color_log(TAG, "   ;1:,,SH:   .A@&&B#&8H#BS,,,,,,,,,.,5XS,     3@MHABM&59M#As..,,,,:,is,   ");
    color_log(TAG, "  .rr,,,;9&1   hBHHBB&8AMGr,,,,,,,,,,,:h&&9s;   r9&BMHBHMB9:  . .,,,,;ri.  ");
    color_log(TAG, "  :1:....:5&XSi;r8BMBHHA9r:,......,,,,:ii19GG88899XHHH&GSr.      ...,:rs.  ");
    color_log(TAG, "  ;s.     .:sS8G8GG889hi.        ....,,:;:,.:irssrriii:,.        ...,,i1,  ");
    color_log(TAG, "  ;1,         ..,....,,isssi;,        .,,.                      ....,.i1,  ");
    color_log(TAG, "  ;h:               i9HHBMBBHAX9:         .                     ...,,,rs,  ");
    color_log(TAG, "  ,1i..            :A#MBBBBMHB##s                             ....,,,;si.  ");
    color_log(TAG, "  .r1,..        ,..;3BMBBBHBB#Bh.     ..                    ....,,,,,i1;   ");
    color_log(TAG, "   :h;..       .,..;,1XBMMMMBXs,.,, .. :: ,.               ....,,,,,,ss.   ");
    color_log(TAG, "    ih: ..    .;;;, ;;:s58A3i,..    ,. ,.:,,.             ...,,,,,:,s1,    ");
    color_log(TAG, "    .s1,....   .,;sh,  ,iSAXs;.    ,.  ,,.i85            ...,,,,,,:i1;     ");
    color_log(TAG, "     .rh: ...     rXG9XBBM#M#MHAX3hss13&&HHXr         .....,,,,,,,ih;      ");
    color_log(TAG, "      .s5: .....    i598X&&A&AAAAAA&XG851r:       ........,,,,:,,sh;       ");
    color_log(TAG, "      . ihr, ...  .         ..                    ........,,,,,;11:.       ");
    color_log(TAG, "         ,s1i. ...  ..,,,..,,,.,,.,,.,..       ........,,.,,.;s5i.         ");
    color_log(TAG, "          .:s1r,......................       ..............;shs,           ");
    color_log(TAG, "          . .:shr:.  ....                 ..............,ishs.             ");
    color_log(TAG, "              .,issr;,... ...........................,is1s;.               ");
    color_log(TAG, "                 .,is1si;:,....................,:;ir1sr;,                  ");
    color_log(TAG, "                    ..:isssssrrii;::::::;;iirsssssr;:..                    ");
    color_log(TAG, "                         .,::iiirsssssssssrri;;:.                          ");
    color_log(TAG, "---------------------------------------------------------------------------");
    color_log(TAG, "");
}
