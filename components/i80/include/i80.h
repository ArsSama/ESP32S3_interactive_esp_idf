#ifndef __I80_H
#define __I80_H

#include <stdint.h>
#include <esp_log.h>
#include <esp_err.h>
#include <driver/gpio.h>
#include <driver/ledc.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_lcd_panel_io.h"
#include "esp_lcd_panel_ops.h"
#include "i80_lcd_panels.h"
#include "lvgl.h"

/*测试3.97代码 start*/
// #define reset 42
// #define sdo 46
// #define sda 39
// #define wr 37
// #define rs 35
// #define cs 33
// #define te 48
// #define data {21,13,11,9,7,5,3,2,1,4,6,8,10,12,14,18}
/*测试3.97代码 end*/

esp_lcd_panel_io_handle_t lcd_i80_bus_io_init(uint16_t pclk_mhz, size_t transfer_size);
void lcd_init_reg(const esp_lcd_panel_io_handle_t io, const lcd_panel_reg_t reg_table[]);
void Lcd_reset(const esp_lcd_panel_io_handle_t io);
void lcd_draw_rect(const esp_lcd_panel_io_handle_t io, uint16_t x_start, uint16_t y_start, uint16_t x_end, uint16_t y_end, const void *color_data);
void lcd_disp_switch(const esp_lcd_panel_io_handle_t io, bool sw, uint16_t bright);
esp_err_t lcd_bl_set(uint8_t brightness);

#endif
