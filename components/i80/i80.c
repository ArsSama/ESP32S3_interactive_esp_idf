/********************************************************************************
 * @File name: i80.c
 * @Author: Letitia-Ron/PU-TONG
 * @Version: 1.1
 * @Date: 2022-12-27
 * @Description: i80总线初始化以及背光设置
 ********************************************************************************/

#include <stdio.h>
#include "i80.h"

static char *TAG = "i80_lcd";

static esp_err_t lcd_bl_init(void);

/**
 * @brief 初始化8080LCD总线
 *
 * @param pclk_mhz 像素时钟，单位为mhz
 * @param transfer_size 单次传输最大数据量，最高为64k
 * @param trans_done_cb 传输完毕后的回调函数，NULL为不使用
 * @return esp_lcd_panel_io_handle_t 显示接口句柄
 */
esp_lcd_panel_io_handle_t lcd_i80_bus_io_init(uint16_t pclk_mhz, size_t transfer_size)
{
    /* 初始化背光 */
    lcd_bl_init();

    /* 初始化8080总线：16位数据，DC与WR脚 */
    ESP_LOGI(TAG, "Initialize Intel 8080 bus");
    esp_lcd_i80_bus_handle_t i80_bus = NULL;
    esp_lcd_i80_bus_config_t bus_config = {
        .clk_src = LCD_CLK_SRC_DEFAULT, // 由于IDF5.0版本的更新，需要添加该代码（LCD时钟） PS:以前4.4版本不用
        .dc_gpio_num = CONFIG_BSP_LCD_DC_PIN,
        .wr_gpio_num = CONFIG_BSP_LCD_WR_PIN, // DC与WR引脚
#if CONFIG_BSP_LCD_CONTROLLER_DATA_WIDTH_8
        .data_gpio_nums = {
            CONFIG_BSP_LCD_DATA8_PIN_0,
            CONFIG_BSP_LCD_DATA8_PIN_1,
            CONFIG_BSP_LCD_DATA8_PIN_2,
            CONFIG_BSP_LCD_DATA8_PIN_3,
            CONFIG_BSP_LCD_DATA8_PIN_4,
            CONFIG_BSP_LCD_DATA8_PIN_5,
            CONFIG_BSP_LCD_DATA8_PIN_6,
            CONFIG_BSP_LCD_DATA8_PIN_7},
#elif CONFIG_BSP_LCD_CONTROLLER_DATA_WIDTH_16
        .data_gpio_nums = {
            CONFIG_BSP_LCD_DATA16_PIN_0,
            CONFIG_BSP_LCD_DATA16_PIN_1,
            CONFIG_BSP_LCD_DATA16_PIN_2,
            CONFIG_BSP_LCD_DATA16_PIN_3,
            CONFIG_BSP_LCD_DATA16_PIN_4,
            CONFIG_BSP_LCD_DATA16_PIN_5,
            CONFIG_BSP_LCD_DATA16_PIN_6,
            CONFIG_BSP_LCD_DATA16_PIN_7,
            CONFIG_BSP_LCD_DATA16_PIN_8,
            CONFIG_BSP_LCD_DATA16_PIN_9,
            CONFIG_BSP_LCD_DATA16_PIN_10,
            CONFIG_BSP_LCD_DATA16_PIN_11,
            CONFIG_BSP_LCD_DATA16_PIN_12,
            CONFIG_BSP_LCD_DATA16_PIN_13,
            CONFIG_BSP_LCD_DATA16_PIN_14,
            CONFIG_BSP_LCD_DATA16_PIN_15},
#endif
#if CONFIG_BSP_LCD_CONTROLLER_DATA_WIDTH_8
        .bus_width = 8, // 总线宽度8位
#elif CONFIG_BSP_LCD_CONTROLLER_DATA_WIDTH_16
        .bus_width = 16,                          // 总线宽度16位
#endif
        .max_transfer_bytes = transfer_size, // 缓冲区大小
    };
    ESP_ERROR_CHECK(esp_lcd_new_i80_bus(&bus_config, &i80_bus));

    /* 初始化LCD面板，使用上述8080总线，并设置CS引脚以及总线频率 */
    esp_lcd_panel_io_handle_t io_handle;
    esp_lcd_panel_io_i80_config_t io_config = {
        .cs_gpio_num = CONFIG_BSP_LCD_CS_PIN, // CS引脚
        .pclk_hz = pclk_mhz * 1000 * 1000,    // 总线时钟频率(单位Mhz)
        .trans_queue_depth = 10,
        .dc_levels = {
            .dc_idle_level = 0,
            .dc_cmd_level = 0,
            .dc_dummy_level = 0,
            .dc_data_level = 1,
        },
        .flags = {
#if CONFIG_BSP_LCD_CONTROLLER_DATA_WIDTH_8
            .swap_color_bytes = !LV_COLOR_16_SWAP, // Swap can be done in LvGL (default) or DMA
#elif CONFIG_BSP_LCD_CONTROLLER_DATA_WIDTH_16
            .swap_color_bytes = LV_COLOR_16_SWAP, // Swap can be done in LvGL (default) or DMA
#endif
        },
#if CONFIG_BSP_LCD_CONTROLLER_DATA_WIDTH_8
        .lcd_cmd_bits = 8, // 总线宽度8位
#elif CONFIG_BSP_LCD_CONTROLLER_DATA_WIDTH_16
        .lcd_cmd_bits = 16,                       // 总线宽度16位
#endif
        .lcd_param_bits = 8, // 指令与指令参数的长度
    };
    ESP_ERROR_CHECK(esp_lcd_new_panel_io_i80(i80_bus, &io_config, &io_handle));

    return io_handle;
}

void Lcd_reset(const esp_lcd_panel_io_handle_t io)
{
    // perform hardware reset
    if (CONFIG_BSP_LCD_REST_PIN >= 0)
    {
        gpio_set_direction(CONFIG_BSP_LCD_REST_PIN, GPIO_MODE_OUTPUT); // 初始化IO模式
        gpio_set_level(CONFIG_BSP_LCD_REST_PIN, 1);
        vTaskDelay(pdMS_TO_TICKS(10));
        gpio_set_level(CONFIG_BSP_LCD_REST_PIN, 0);
        vTaskDelay(pdMS_TO_TICKS(10));
        gpio_set_level(CONFIG_BSP_LCD_REST_PIN, 1);
        vTaskDelay(pdMS_TO_TICKS(120));
    }
    else
    {                                  // perform software reset
        vTaskDelay(pdMS_TO_TICKS(20)); // spec, wait at least 5m before sending new command
    }
    ESP_LOGI(TAG, "Lcd_reset ok");
}

void lcd_init_reg(const esp_lcd_panel_io_handle_t io, const lcd_panel_reg_t reg_table[])
{
    uint8_t i = 0;
    while (reg_table[i].len != 0xFF)
    {
        esp_lcd_panel_io_tx_param(io, reg_table[i].reg, reg_table[i].val, reg_table[i].len);
        i++;
    }
}

void lcd_draw_rect(const esp_lcd_panel_io_handle_t io, uint16_t x_start, uint16_t y_start, uint16_t x_end, uint16_t y_end, const void *color_data)
{
    // esp_lcd_panel_io_tx_param(io, 0x11, NULL, 0);
    esp_lcd_panel_io_tx_param(io, 0x2A, (uint8_t[]){(x_start >> 8) & 0xFF, x_start & 0xFF, (x_end >> 8) & 0xFF, x_end & 0xFF}, 4);
    esp_lcd_panel_io_tx_param(io, 0x2B, (uint8_t[]){(y_start >> 8) & 0xFF, y_start & 0xFF, (y_end >> 8) & 0xFF, y_end & 0xFF}, 4);

    size_t len = (x_end - x_start + 1) * (y_end - y_start + 1) * sizeof(uint16_t);
    esp_lcd_panel_io_tx_color(io, 0x2C, color_data, len);
}

void lcd_disp_switch(const esp_lcd_panel_io_handle_t io, bool sw, uint16_t bright)
{
    if (sw)
    {
        esp_lcd_panel_io_tx_param(io, 0x29, NULL, 0); // 开显示
        lcd_bl_set(bright);                           // 打开背光
        ESP_LOGI(TAG, "display on bright:%d %%", bright);
    }
    else
    {
        esp_lcd_panel_io_tx_param(io, 0x28, NULL, 0); // 关显示
        ESP_LOGI(TAG, "display off");
    }
}

/* 定时器配置 */
static const ledc_timer_config_t lcd_bl_ledc_timer = {
    .duty_resolution = LEDC_TIMER_10_BIT, // LEDC驱动器占空比精度
    .freq_hz = 1000,                      // PWM频率
    .speed_mode = LEDC_LOW_SPEED_MODE,
    .timer_num = LEDC_TIMER_0, // ledc使用的定时器编号。若需要生成多个频率不同的PWM信号，则需要指定不同的定时器
    .clk_cfg = LEDC_AUTO_CLK,  // 自动选择定时器的时钟源
};

/* ledc通道配置 */
static const ledc_channel_config_t lcd_bl_ledc_channel = {
    .channel = LEDC_CHANNEL_0,         // LCD背光使用通道0
    .duty = 0,                         // 占空比0
    .gpio_num = CONFIG_BSP_LCD_BL_PIN, // 连接背光的IO
    .speed_mode = LEDC_LOW_SPEED_MODE,
    .hpoint = 0,
    .timer_sel = LEDC_TIMER_0, // 使用上面初始化过的定时器
};

/**
 * @brief 初始化LCD背光PWM
 *
 * @return esp_err_t 错误代码
 */
static esp_err_t lcd_bl_init(void)
{
    /* 初始化定时器1，将初始化好的定时器编号传给ledc通道初始化函数即可 */
    esp_err_t error = ledc_timer_config(&lcd_bl_ledc_timer);
    if (error)
    {
        return error;
    }

    /* 初始化ledc通道 */
    return ledc_channel_config(&lcd_bl_ledc_channel);
}

/**
 * @brief 设置背光亮度
 *
 * @param brightness 背光百分比，0-100
 * @return esp_err_t 错误代码
 */
esp_err_t lcd_bl_set(uint8_t brightness)
{
    /* 设定PWM占空比 */
    esp_err_t error = ledc_set_duty(lcd_bl_ledc_channel.speed_mode, lcd_bl_ledc_channel.channel, brightness * 10);
    if (error)
    {
        return error;
    }

    /* 更新PWM占空比输出 */
    return ledc_update_duty(lcd_bl_ledc_channel.speed_mode, lcd_bl_ledc_channel.channel);
}
