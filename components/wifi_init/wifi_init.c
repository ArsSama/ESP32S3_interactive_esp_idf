/********************************************************************************
 * @File name: wifi_init.c
 * @Author: Letitia-Ron/PU-TONG/ArsSama
 * @Version: 1.0
 * @Date: 2023-4-15
 * @Description: wifi_init
 ********************************************************************************/

#include "wifi_init.h"

static const char *TAG = "wifi_init";

/**
 * @brief wifi事件回调
 *
 * @param arg
 * @param event_base
 * @param event_id
 * @param event_data
 */
static void event_handler(void *arg, esp_event_base_t event_base,
                          int32_t event_id, void *event_data)
{
    if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_START)
    {
        esp_wifi_connect();
    }
    else if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_DISCONNECTED)
    {
        esp_wifi_connect();
    }
    else if (event_base == IP_EVENT && event_id == IP_EVENT_STA_GOT_IP)
    {
        ip_event_got_ip_t *event = (ip_event_got_ip_t *)event_data;
        ESP_LOGI(TAG, "got ip:" IPSTR, IP2STR(&event->ip_info.ip));
    }
}

/**
 * @brief wifi作为STA模式设置扫描
 *
 */
static void fast_scan(void)
{
    ESP_ERROR_CHECK(esp_netif_init());
    ESP_LOGI(TAG, "esp_netif_init");
    ESP_ERROR_CHECK(esp_event_loop_create_default());
    ESP_LOGI(TAG, "esp_event_loop_create_default");

    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(esp_wifi_init(&cfg));
    ESP_LOGI(TAG, "esp_wifi_init");

    ESP_ERROR_CHECK(esp_event_handler_instance_register(WIFI_EVENT, ESP_EVENT_ANY_ID, &event_handler, NULL, NULL));
    ESP_ERROR_CHECK(esp_event_handler_instance_register(IP_EVENT, IP_EVENT_STA_GOT_IP, &event_handler, NULL, NULL));
    ESP_LOGI(TAG, "esp_event_handler_instance_register");

    // Initialize default station as network interface instance (esp-netif)
    esp_netif_t *sta_netif = esp_netif_create_default_wifi_sta();
    assert(sta_netif);
    ESP_LOGI(TAG, "assert");

    // Initialize and start WiFi
    wifi_config_t wifi_config = {
        .sta = {
            .ssid = DEFAULT_SSID,
            .password = DEFAULT_PWD,
            .scan_method = DEFAULT_SCAN_METHOD,
            .sort_method = DEFAULT_SORT_METHOD,
            .threshold.rssi = DEFAULT_RSSI,
            .threshold.authmode = DEFAULT_AUTHMODE,
        },
    };
    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA));
    ESP_LOGI(TAG, "esp_wifi_set_mode STA");
    ESP_ERROR_CHECK(esp_wifi_set_config(WIFI_IF_STA, &wifi_config));
    ESP_LOGI(TAG, "esp_wifi_set_config STA");
    ESP_ERROR_CHECK(esp_wifi_start());
    ESP_LOGI(TAG, "esp_wifi_start");
}

/**
 * @brief wifi_init初始化并连接wifi
 *
 * @param void
 *
 */
void wifi_init(void)
{

    // Initialize NVS
    esp_err_t ret = nvs_flash_init();
    if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND)
    {
        ESP_ERROR_CHECK(nvs_flash_erase());
        ret = nvs_flash_init();
    }
    ESP_ERROR_CHECK(ret);
    fast_scan();

    /*上面的扫描就不用了，下面直接连接指定wifi,简洁*/
    // ESP_ERROR_CHECK(nvs_flash_init());
    // ESP_ERROR_CHECK(esp_netif_init());
    // ESP_ERROR_CHECK(esp_event_loop_create_default());
    // ESP_ERROR_CHECK(example_connect());

    /*等待连接成功*/
    while (get_wifi_status() != ESP_OK) // 确定wifi连接上才进行下面的创建任务
    {
        vTaskDelay(pdMS_TO_TICKS(500));
    }
    ESP_LOGW(TAG, "WIFI Connect Successfully!");
}

/**
 * @brief Get the wifi status object 获取WiFi连接状态
 *
 * @return esp_err_t -ESP_OK is connect
 */
esp_err_t get_wifi_status(void)
{
    wifi_ap_record_t wifi_ap; /*定义结构体记录连接的wifi的信息*/
    esp_err_t ret = esp_wifi_sta_get_ap_info(&wifi_ap);
    if (ret != ESP_OK)
    {
        ESP_LOGE(TAG, "wifi_disconnect");
        return ret; /*未连接，返回错误码*/
    }
    /*已经连接，赋值信号强度*/
    ESP_LOGW(TAG, "wifi_rssi: %d", wifi_ap.rssi); /*打印信号强度*/

    return ret;
}

/**
 * @brief wifi_reconnect--wifi重连（可自定义参数）
 *
 * @param set 是否设置WiFi参数
 * @param ssid WiFi名称
 * @param password WiFi密码
 * @return esp_err_t
 */
esp_err_t wifi_reconnect(bool set, const char *ssid, const char *password)
{
    if (set)
    {
        ESP_ERROR_CHECK(esp_wifi_disconnect()); /* 断开当前连接的wifi */
        ESP_LOGW(TAG, "try reset other wifi");
        ESP_LOGI(TAG, "WIFIName:%s   WIFIPW:%s", ssid, password);
        wifi_config_t wifi_config = {
            .sta = {
                .ssid = "",
                .password = ""},
        };                                                  /*这里就设置个空的结构体，方便下面赋值传参*/
        strcpy((char *)wifi_config.sta.ssid, ssid);         /*在进行字符串复制之前，需要将结构体成员强制转换为 char* 类型，以满足 strcpy 函数的要求*/
        strcpy((char *)wifi_config.sta.password, password); /*在进行字符串复制之前，需要将结构体成员强制转换为 char* 类型，以满足 strcpy 函数的要求*/
        ESP_LOGI(TAG, "Connecting to %s...", wifi_config.sta.ssid);
        ESP_ERROR_CHECK(esp_wifi_set_config(WIFI_IF_STA, &wifi_config));
        esp_err_t ret = esp_wifi_connect();
        if (ret != ESP_OK)
        {
            ESP_LOGE(TAG, "WiFi connect failed! ret:%x", ret);
            return ret;
        }
    }
    else
    {
        ESP_LOGW(TAG, "try reconnect wifi");
        esp_err_t ret = esp_wifi_connect();
        while (ret != ESP_OK) /*wifi重连原本wifi*/
        {
            ESP_LOGE(TAG, "WiFi connect failed! ret:%x", ret);
            return ret;
        }
    }
    return ESP_OK;
}
