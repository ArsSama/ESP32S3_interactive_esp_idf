/********************************************************************************
 * @File name: generic_gpio.c
 * @Author: Letitia-Ron/PU-TONG/ArsSama
 * @Version: 1.0
 * @Date: 2023-4-15
 * @Description: generic_gpio
 ********************************************************************************/

#include "generic_gpio.h"

static const char *TAG = "KEY_FIFIO";

/* OUTPUT IO——define */
#define GPIO_OUTPUT_IO_0 CONFIG_GPIO_OUTPUT_0
#define GPIO_OUTPUT_IO_1 CONFIG_GPIO_OUTPUT_1
#define GPIO_OUTPUT_PIN_SEL ((1ULL << GPIO_OUTPUT_IO_0) | (1ULL << GPIO_OUTPUT_IO_1))
/* INPUT IO——define */
#define GPIO_INPUT_IO_0 CONFIG_GPIO_INPUT_0
#define GPIO_INPUT_IO_1 CONFIG_GPIO_INPUT_1
#define GPIO_INPUT_PIN_SEL ((1ULL << GPIO_INPUT_IO_0) | (1ULL << GPIO_INPUT_IO_1))
#define ESP_INTR_FLAG_DEFAULT 0

static QueueHandle_t gpio_evt_queue = NULL;

static void IRAM_ATTR gpio_isr_handler(void *arg)
{
    uint32_t gpio_num = (uint32_t)arg;
    xQueueSendFromISR(gpio_evt_queue, &gpio_num, NULL);
}

static void gpio_task(void *pvParameters)
{
    uint32_t io_num;
    for (;;)
    {
        if (xQueueReceive(gpio_evt_queue, &io_num, portMAX_DELAY))
        {
            printf("GPIO[%" PRIu32 "] intr, val: %d\n", io_num, gpio_get_level(io_num));
        }
    }
}

void generic_gpio_init(void)
{
    // 初始化配置结构体
    gpio_config_t io_conf = {};
    // 禁用中断
    io_conf.intr_type = GPIO_INTR_DISABLE;
    // 设置为输出模式
    io_conf.mode = GPIO_MODE_OUTPUT;
    // 您要设置的引脚的位掩码，例如gpi018 /19
    io_conf.pin_bit_mask = GPIO_OUTPUT_PIN_SEL;
    // 禁用下拉模式
    io_conf.pull_down_en = 0;
    // 禁用上拉模式
    io_conf.pull_up_en = 0;
    // 使用给定的设置配置GPIO
    gpio_config(&io_conf);

    // 中断触发-上升边沿
    io_conf.intr_type = GPIO_INTR_POSEDGE;
    // 引脚的位掩码，这里使用GPI04/5
    io_conf.pin_bit_mask = GPIO_INPUT_PIN_SEL;
    // 设置为输入模式
    io_conf.mode = GPIO_MODE_INPUT;
    // 启用上拉模式
    io_conf.pull_up_en = 1;
    gpio_config(&io_conf);

    // 修改一个引脚的gpio中断类型
    gpio_set_intr_type(GPIO_INPUT_IO_0, GPIO_INTR_ANYEDGE);

    // 创建一个队列来处理来自isr的gpio事件
    gpio_evt_queue = xQueueCreate(10, sizeof(uint32_t));
    // 启动gpio任务
    xTaskCreate(gpio_task, "gpio_task", 2048, NULL, 10, NULL);

    // 安装gpio isr服务
    gpio_install_isr_service(ESP_INTR_FLAG_DEFAULT);
    // 为特定的gpio引脚挂载isr处理程序
    gpio_isr_handler_add(GPIO_INPUT_IO_0, gpio_isr_handler, (void *)GPIO_INPUT_IO_0);
    // 为特定的gpio引脚挂载isr处理程序
    gpio_isr_handler_add(GPIO_INPUT_IO_1, gpio_isr_handler, (void *)GPIO_INPUT_IO_1);

    // 删除gpio编号的isr处理程序
    gpio_isr_handler_remove(GPIO_INPUT_IO_0);
    // 再次为特定的gpio引脚挂载isr处理程序
    gpio_isr_handler_add(GPIO_INPUT_IO_0, gpio_isr_handler, (void *)GPIO_INPUT_IO_0);

    printf("Minimum free heap size: %" PRIu32 " bytes\n", esp_get_minimum_free_heap_size());

    vTaskDelay(250 / portTICK_PERIOD_MS);
    // int cnt = 0;
    // while (1)
    // {
    //     printf("cnt: %d\n", cnt++);
    //     vTaskDelay(1000 / portTICK_PERIOD_MS);
    //     gpio_set_level(GPIO_OUTPUT_IO_0, cnt % 2);
    //     gpio_set_level(GPIO_OUTPUT_IO_1, cnt % 2);
    // }
}
