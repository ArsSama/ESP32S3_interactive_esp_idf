#ifndef _KEY_FIFO_H
#define _KEY_FIFO_H

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <inttypes.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "driver/gpio.h"

void generic_gpio_init(void);

#endif

