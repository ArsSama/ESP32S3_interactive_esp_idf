#ifndef __SNTP_CUSTOM_H
#define __SNTP_CUSTOM_H

#include "sntp.h"
#include "esp_log.h"
#include "esp_sntp.h"
#include <time.h>
/*mutex*/
#include "mutex.h"

struct local_time_data
{
    uint8_t local_hour;
    uint8_t local_min;
    uint8_t local_sec;
};

extern struct local_time_data local_time;
// extern uint8_t local_hour;
// extern uint8_t local_min;
// extern uint8_t local_sec;

void get_local_time_task(void *arg);

#endif