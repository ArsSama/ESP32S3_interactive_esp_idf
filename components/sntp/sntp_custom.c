/********************************************************************************
 * @File name: sntp_custom.c
 * @Author: Letitia-Ron/PU-TONG/ArsSama
 * @Version: 1.0
 * @Date: 2023-4-15
 * @Description: sntp_custom
 ********************************************************************************/

#include "sntp_custom.h"
#include "esp_task_wdt.h"

static const char *TAG = "sntp";
static const char *LOCAL_TAG = "local_time";

struct local_time_data local_time;

void obtain_time(void)
{
    ESP_LOGI(TAG, "Initializing SNTP");
    sntp_setoperatingmode(SNTP_OPMODE_POLL);
    sntp_setservername(0, "pool.ntp.org");
    sntp_init();

    // wait for time to be set
    time_t now = 0;
    struct tm timeinfo = {0};
    int retry = 0;
    const int retry_count = 10;
    while (timeinfo.tm_year < (2020 - 1900) && ++retry < retry_count)
    {
        ESP_LOGI(TAG, "Waiting for system time to be set... (%d/%d)", retry, retry_count);
        vTaskDelay(2000 / portTICK_PERIOD_MS);
        time(&now);
        localtime_r(&now, &timeinfo);
    }

    // set timezone
    setenv("TZ", "CST-8", 1);
    tzset();
}

/**
 * @brief ntp_local_init -NTP时间同步到本地
 *
 */
static void ntp_local_init(void)
{
    obtain_time();
    time_t now;
    struct tm timeinfo;
    time(&now);
    localtime_r(&now, &timeinfo);
    ESP_LOGI(TAG, "Current time: %s", asctime(&timeinfo));
}

/**
 * @brief Get the local time task object -获取本地
 *
 * @param arg
 */
void get_local_time_task(void *arg)
{
    // ESP_ERROR_CHECK(esp_task_wdt_add(NULL)); // 为当前任务启用看门狗定时器
    TickType_t xLastWakeTime = xTaskGetTickCount(); /*第一次运行时获取当前系统时间*/

    ntp_local_init(); /*NTP时间同步到本地*/
    time_t now;
    struct tm timeinfo;
    while (1)
    {
        time(&now);
        localtime_r(&now, &timeinfo);
        ESP_LOGI(LOCAL_TAG, "Current local time: %s", asctime(&timeinfo));

        xSemaphoreTake(local_time_mutex, portMAX_DELAY); // 获取本地时间的互斥锁
        /*访问共享资源-本地时间*/
        local_time.local_hour = timeinfo.tm_hour;
        local_time.local_min = timeinfo.tm_min;
        local_time.local_sec = timeinfo.tm_sec;
        ESP_LOGI(LOCAL_TAG, "%d:%d:%d", local_time.local_hour, local_time.local_min, local_time.local_sec);
        xSemaphoreGive(local_time_mutex); // 释放互斥锁

        // ESP_ERROR_CHECK(esp_task_wdt_reset()); // 重置任务看门狗定时器
        vTaskDelayUntil(&xLastWakeTime, pdMS_TO_TICKS(1000)); /*每次运行时延迟 1000ms，以确保任务能够及时释放 CPU 资源*/
        // vTaskDelay(pdMS_TO_TICKS(1000));
        // vTaskDelay(2000 / portTICK_PERIOD_MS);
    }
}
