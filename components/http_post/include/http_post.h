#ifndef __HTTP_POST_H
#define __HTTP_POST_H

#include <inttypes.h>
#include <stdio.h>
#include "esp_log.h"
#include "esp_http_client.h"
#include "wifi_init.h"

#define ONE_NET_API_KEY "rnhxjvOf9H1U8CGVbb6Kd1bH9V4=" /*填入你的APIKEY*/
#define ONE_NET_DEVICE_ID "1058641170"                 /*填入你的设备ID*/

DRAM_ATTR extern float post_data; /*post 测试数据*/

// void http_post_onenet(float temp, float humi);
esp_err_t net_ping(void);
void http_post_task(void *arg);
void http_post_timer_init(void); /*初始化http post定时器*/

#endif