/********************************************************************************
 * @File name: http_post.c
 * @Author: Letitia-Ron/PU-TONG/ArsSama
 * @Version: 1.1
 * @Date: 2023-4-15
 * @Description: http_post
 ********************************************************************************/

#include "http_post.h"

static const char *TAG = "HTTP_POST";

static uint8_t post_err_count = 0; /*post失败计时*/
DRAM_ATTR float post_data;                   /*post 测试数据*/

/**
 * @brief net_ping -tcp网络测试 ESP_OK--successful
 *
 * @return esp_err_t
 */
esp_err_t net_ping(void)
{
    esp_http_client_config_t config = {
        .url = "http://www.baidu.com", // 待测试的目标 URL
    };
    esp_http_client_handle_t client = esp_http_client_init(&config);

    esp_err_t err = esp_http_client_perform(client);
    if (err != ESP_OK)
    {
        ESP_LOGE(TAG, "Failed to perform HTTP request");
        esp_http_client_cleanup(client);
        return ESP_FAIL;
    }
    else
    {
        ESP_LOGW(TAG, "Perform HTTP request successfully");
        esp_http_client_cleanup(client);
        return ESP_OK;
    }
    // vTaskDelay(pdMS_TO_TICKS(5000)); // 每隔 5 秒进行一次 HTTP 请求
}

/**
 * @brief http_post_onenet
 *
 * @param temp
 * @param humi
 */
static void http_post_onenet(float temp, float humi) // 笔记，用指针传参
{
    char data_buf[128];
    // sprintf(data_buf, "{\"datastreams\":[{\"id\":\"temp\",\"datapoints\":[{\"value\":%.2f}]}]}", temp);
    sprintf(data_buf, "{\"datastreams\":[{\"id\":\"temp\",\"datapoints\":[{\"value\":%.2f}]},{\"id\":\"humi\",\"datapoints\":[{\"value\":%.2f}]}]}", temp, humi);

    esp_http_client_config_t config = {
        .url = "http://api.heclouds.com/devices/" ONE_NET_DEVICE_ID "/datapoints",
        .method = HTTP_METHOD_POST,
        .event_handler = NULL,
        .user_data = NULL,
    };

    esp_http_client_handle_t client = esp_http_client_init(&config);
    esp_http_client_set_header(client, "api-key", ONE_NET_API_KEY);
    esp_http_client_set_header(client, "Content-Type", "application/json");
    esp_http_client_set_post_field(client, data_buf, strlen(data_buf)); // post data
    esp_err_t err = esp_http_client_perform(client);
    if (err == ESP_OK)
    {
        ESP_LOGI(TAG, "HTTP POST Status = %d, content_length = %" PRId64 "",
                 esp_http_client_get_status_code(client),
                 esp_http_client_get_content_length(client));
    }
    else
    {
        ESP_LOGE(TAG, "HTTP POST request failed: %s,Post_err_count:%d", esp_err_to_name(err), post_err_count + 1);

        post_err_count += 1;
        if ((post_err_count >= 4) && (net_ping() != ESP_OK)) /*断网超时处理,12s*/
        {
            post_err_count = 0;
            ESP_LOGW(TAG, "wifi disconnect!");
            // Disconnect Wi-Fi
            esp_wifi_disconnect();
            // Wait for disconnection
            while (get_wifi_status() == ESP_OK)
            {
                ESP_LOGW(TAG, "Wait for disconnection!");
                vTaskDelay(pdMS_TO_TICKS(500));
            }
            // ESP_ERROR_CHECK(wifi_reconnect(false, NULL, NULL));
            // Reconnect Wi-Fi
            ESP_LOGW(TAG, "wifi reconnect!");
            esp_wifi_connect();
        }
    }
    /* 清理资源 */
    esp_http_client_cleanup(client);
}

/**
 * @brief http_post_task -http post CPU任务
 *
 * @param arg
 */
void http_post_task(void *arg)
{
    // ESP_ERROR_CHECK(esp_task_wdt_add(NULL)); // 为当前任务启用看门狗定时器
    // TickType_t xLastWakeTime = xTaskGetTickCount(); /*第一次运行时获取当前系统时间*/
    post_data = 10.0;
    while (1)
    {
        post_data += 0.5;
        if (post_data >= 90)
        {
            post_data = 0;
        }
        // if (get_wifi_status() == ESP_OK)
        // {
        http_post_onenet(post_data, post_data + 10);
        // }

        // ESP_ERROR_CHECK(esp_task_wdt_reset()); // 重置任务看门狗定时器
        // vTaskDelayUntil(&xLastWakeTime, pdMS_TO_TICKS(2000)); /*每次运行时延迟 1000ms，以确保任务能够及时释放 CPU 资源*/
        vTaskDelay(pdMS_TO_TICKS(3000));
        // vTaskDelay(2000 / portTICK_PERIOD_MS);
    }
}
