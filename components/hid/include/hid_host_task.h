#ifndef __HID_HOST_TASK_H__
#define __HID_HOST_TASK_H__

#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_err.h"
#include "esp_log.h"
#include "usb/usb_host.h"
#include "errno.h"
#include "driver/gpio.h"

#include "hid_host.h"
#include "hid_usage_keyboard.h"
#include "hid_usage_mouse.h"

/*mouse data struct*/
struct mouse_data_struct
{
    int x;
    int y;
    int button1;
    int button2;
};
extern struct  mouse_data_struct mouse_data;/*用来传输鼠标数据给lvgl*/

void hid_host_task(void *arg);

#endif