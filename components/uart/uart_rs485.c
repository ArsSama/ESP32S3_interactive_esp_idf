/********************************************************************************
 * @File name: uart_rs485.c
 * @Author: Letitia-Ron/PU-TONG/ArsSama
 * @Version: 1.0
 * @Date: 2023-4-15
 * @Description: uart_rs485
 ********************************************************************************/

#include "uart_rs485.h"

#define TAG "RS485_ECHO_APP"

/*风向问询帧*/
uint8_t wind_ask[] = {0x01, 0x03, 0x00, 0x0A, 0x00, 0x01, 0xED, 0xC9};
/*风速问询帧*/
uint8_t wind_velocity_ask[] = {0x01, 0x03, 0x00, 0x09, 0x00, 0x01, 0x54, 0x08};
/*雨雪问询帧*/
uint8_t sleet_ask[] = {0x01, 0x03, 0x00, 0x16, 0x00, 0x01, 0xC5, 0x58};

static void echo_send(const int port, const char *str, uint8_t length)
{
    if (uart_write_bytes(port, str, length) != length)
    {
        ESP_LOGE(TAG, "Send data critical failure.");
        // add your code to handle sending failure here
        abort();
    }
}

// An example of echo test with hardware flow control on UART
void echo_task(void *arg)
{
    const int uart_num = ECHO_UART_PORT;
    uart_config_t uart_config = {
        .baud_rate = BAUD_RATE,
        .data_bits = UART_DATA_8_BITS,
        .parity = UART_PARITY_DISABLE,
        .stop_bits = UART_STOP_BITS_1,
        .flow_ctrl = UART_HW_FLOWCTRL_DISABLE,
        .rx_flow_ctrl_thresh = 122,
        .source_clk = UART_SCLK_DEFAULT,
    };

    // Set UART log level
    esp_log_level_set(TAG, ESP_LOG_INFO);

    ESP_LOGI(TAG, "Start RS485 application test and configure UART.");

    // Install UART driver (we don't need an event queue here)
    // In this example we don't even use a buffer for sending data.
    ESP_ERROR_CHECK(uart_driver_install(uart_num, BUF_SIZE * 2, 0, 0, NULL, 0));

    // Configure UART parameters
    ESP_ERROR_CHECK(uart_param_config(uart_num, &uart_config));

    ESP_LOGI(TAG, "UART set pins, mode and install driver.");

    // Set UART pins as per KConfig settings
    ESP_ERROR_CHECK(uart_set_pin(uart_num, ECHO_TEST_TXD, ECHO_TEST_RXD, ECHO_TEST_RTS, ECHO_TEST_CTS));

    // Set RS485 half duplex mode
    ESP_ERROR_CHECK(uart_set_mode(uart_num, UART_MODE_RS485_HALF_DUPLEX));

    // Set read timeout of UART TOUT feature
    ESP_ERROR_CHECK(uart_set_rx_timeout(uart_num, ECHO_READ_TOUT));

    // Allocate buffers for UART
    uint8_t *data = (uint8_t *)malloc(BUF_SIZE);

    ESP_LOGI(TAG, "UART start recieve loop.\r\n");
    echo_send(uart_num, "Start RS485 UART test.\r\n", 24);

    while (1)
    {
        // Read data from UART
        int len = uart_read_bytes(uart_num, data, BUF_SIZE, PACKET_READ_TICS);

        // Write data back to UART
        if (len > 0)
        {
            ESP_LOGI(TAG, "Received %u bytes:", len);
            printf("[ ");
            for (int i = 0; i < len; i++)
            {
                printf("0x%.2X ", (uint8_t)data[i]);
                echo_send(uart_num, (const char *)&data[i], 1);
                // Add a Newline character if you get a return charater from paste (Paste tests multibyte receipt/buffer)
                if (data[i] == '\r')
                {
                    echo_send(uart_num, "\n", 1);
                }
            }
            printf("] \n");

            /*debug send data*/
            for (uint8_t i = 0; i < 8; i++)
            {
                // echo_send(uart_num, (const char *)&wind_ask[i], 1);
                uart_write_bytes(uart_num, (const char *)&wind_ask[i], 1);
            }
        }
        else
        {
            // Echo a "." to show we are alive while we wait for input
            // echo_send(uart_num, ".", 1);
            ESP_ERROR_CHECK(uart_wait_tx_done(uart_num, 10));
        }
    }
    vTaskDelete(NULL);
}

#include "esp32s3/rom/crc.h"
#include <stdio.h>

#define DATA_LEN 6

void crc_test(void)
{
    uint8_t data[8] = {0x01, 0x03, 0x00, 0x09, 0x00, 0x01, 0x54, 0x08};
    uint8_t data1[7] = {0x01, 0x03, 0x02, 0x00, 0x17, 0xF8, 0x4A};
    uint16_t crc_result_h_le = 0;
    uint16_t crc_result_h_be = 0;
    /*硬件CRC---小端还是大端模式进行计算crc*/
    crc_result_h_le = crc16_le(0, data, DATA_LEN);  // 使用little-endian格式计算32位CRC值
    crc_result_h_be = crc16_be(0, data1, DATA_LEN); // 使用big-endian格式计算32位CRC值
    printf("CRC16_h_le: 0x%d\nCRC16_h_be: 0x%d\n", crc_result_h_le, crc_result_h_be);
}

/*麻麻地，mobus一定要用矩阵按键的那种event_handler写法，高级高级*/
