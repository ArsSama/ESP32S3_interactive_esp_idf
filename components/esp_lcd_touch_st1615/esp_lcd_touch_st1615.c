/********************************************************************************
 * @File name: esp_lcd_touch_st1615.c
 * @Author: Letitia-Ron/PU-TONG/ArsSama
 * @Version: 1.0
 * @Date: 2023-4-15
 * @Description: esp_lcd_touch_st1615
 ********************************************************************************/

#include "esp_lcd_touch_st1615.h"
#include <stdio.h>
#include "esp_log.h"
#include "driver/i2c.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"

static const char *TAG = "touch_st1615";

/* debug logi */
#define ESP_DEBUG_LOGI 1
#define ST1615_TEST 0

/* init Resolution */
#define X_Resolution 320
#define Y_Resolution 320

#define Firmware_Version 0x00
#define Status_Reg 0x01 /*状态寄存器*/
#define Device_Control_Reg 0x02
#define Timeout_to_Idle_Reg 0x03

/*XY_Resolution -- XY分辨率寄存器表示触摸屏X坐标和Y坐标的分辨率。*/
#define XY_Resolution_High_Byte 0x04 /*Reserved(7Bit) X_Res_H_RO(6\5\4Bit) Reserved(3Bit) Y_Res_H_RO(2\1\0Bit) */
#define X_Resolution_Low_Byte 0x05   /*X_Res_L RO*/
#define Y_Resolution_Low_Byte 0x06   /*Y_Res_L RO*/

#define Advanced_Touch_Info 0x10 /*Advanced_Touch_Info(RO)*/
#define Keys_Reg 0x11            /*Keys_Reg(RO)*/

/*XY0_Coord -- 0触摸点的寄存器*/
#define XY0_Coord_High_Byte 0x12 /*指触摸点0的X和Y坐标的高8位字节;;X坐标的高8位字节存储在高4位中，Y坐标的高8位字节存储在低4位中,触摸点有效位都在X或者Y的最高位，剩下三位是坐标数据*/
#define X0_Coord_Low_Byte 0x13   /*触摸点0的水平坐标的低位字节*/
#define Y0_Coord_Low_Byte 0x14   /*触摸点0的垂直坐标的低位字节*/

static esp_err_t st1615_status(uint8_t *status); // ST1615芯片状态
static esp_err_t st1615_init(void);              // 初始化ST1615触摸芯片

// 定义I2C参数
#define ST1615_I2C_PORT (0)
#define ST1615_I2C_ADDR (0x55) /* 根据数据手册定义ST1615的寄存器地址 */
#define ST1615_SCL_PIN (21)
#define ST1615_SDA_PIN (17)

#if ST1615_TEST
/**
 * @brief printf_bin--打印整数的二进制数据
 *
 * @param num
 */
static void printf_bin(int num)
{
    int i, j, k;
    unsigned char *p = (unsigned char *)&num + 3; // p先指向num后面第3个字节的地址，即num的最高位字节地址

    for (i = 0; i < 4; i++) // 依次处理4个字节(32位）
    {
        j = *(p - i);                // 取每个字节的首地址，从高位字节到低位字节，即p p-1 p-2 p-3地址处
        for (int k = 7; k >= 0; k--) // 处理每个字节的8个位，注意字节内部的二进制数是按照人的习惯存储！
        {
            if (j & (1 << k)) // 1左移k位，与单前的字节内容j进行或运算，如k=7时，00000000&10000000=0 ->该字节的最高位为0
                printf("1");
            else
                printf("0");
        }
        printf(" "); // 每8位加个空格，方便查看
    }
    printf("\r\n");
}
#endif

/**
 * @brief st1615写寄存器数据
 *
 * @param reg_addr
 * @param data
 * @return esp_err_t
 */
static esp_err_t st1615_register_write_byte(uint8_t reg_addr, uint8_t data)
{
    assert(reg_addr != NULL);

    uint8_t ret = ESP_OK;
    uint8_t write_buf[2] = {reg_addr, data};
    ret |= i2c_master_write_to_device(ST1615_I2C_PORT, ST1615_I2C_ADDR, write_buf, sizeof(write_buf), 1000 / portTICK_PERIOD_MS);
    return ret;
}

/**
 * @brief st1615读取数据长度
 *
 * @param reg_addr
 * @param data
 * @param len
 * @return esp_err_t
 */
static esp_err_t st1615_read_len(uint16_t reg_addr, uint8_t *data, uint8_t len)
{
    assert(reg_addr != NULL);

    uint8_t ret = ESP_OK;
    ret |= i2c_master_write_read_device(ST1615_I2C_PORT, ST1615_I2C_ADDR, &reg_addr, 1, data, len, 1000 / portTICK_PERIOD_MS);
    return ret;
}

/**
 * @brief st1615初始化
 *
 * @return esp_err_t
 */
esp_err_t esp_lcd_touch_new_i2c_st1615(void)
{
    esp_err_t ret = ESP_OK;

    ret |= gpio_set_direction(18, GPIO_MODE_INPUT);

    // 控制RST引脚输出低电平
    gpio_config_t io_conf;
    io_conf.mode = GPIO_MODE_INPUT_OUTPUT;
    io_conf.pull_down_en = GPIO_PULLDOWN_DISABLE;
    io_conf.pull_up_en = GPIO_PULLUP_DISABLE;
    io_conf.intr_type = GPIO_INTR_DISABLE;
    io_conf.pin_bit_mask = (1ULL << 48);
    ret |= gpio_config(&io_conf);

    ret |= gpio_set_level(48, 0);
    // 等待一段时间，确保芯片被复位
    vTaskDelay(pdMS_TO_TICKS(10));
    // 控制RST引脚输出高电平
    ret |= gpio_set_level(48, 1);

    return ret;
}

static esp_err_t st1615_status(uint8_t *status)
{
    switch ((*status & 0x0f))
    {
    case 0:
        ESP_LOGI(TAG, "st1615 status: Normal!");
        break;
    case 1:
        ESP_LOGW(TAG, "st1615 status: Init!");
        break;
    case 3:
        ESP_LOGW(TAG, "st1615 status: Auto Turning!");
        break;
    case 5:
        ESP_LOGW(TAG, "st1615 status: Power Down!");
        break;
    case 6:
        ESP_LOGW(TAG, "st1615 status: Boot ROM!");
        break;
    case 7:
        ESP_LOGW(TAG, "st1615 status: Waiting to execute Sub-AP!");
        break;
    default:
        ESP_LOGE(TAG, "st1615 status read error!");
        break;
    }

    switch ((*status & 0xf0) >> 4)
    {
    case 0:
        ESP_LOGI(TAG, "st1615 error code: No Error!");
        break;
    case 1:
        ESP_LOGW(TAG, "st1615 error code: Invalid Address!");
        break;
    case 2:
        ESP_LOGW(TAG, "st1615 error code: Invalid Value!");
        break;
    case 3:
        ESP_LOGW(TAG, "st1615 error code: Invalid Platform!");
        break;
    case 4:
        ESP_LOGW(TAG, "st1615 error code: Dev Not Found!");
        break;
    case 5:
        ESP_LOGW(TAG, "st1615 error code: Stack Overflow!");
        break;
    case 6:
        ESP_LOGW(TAG, "st1615 error code: Invalid Firmware Parameter Table!");
        break;
    case 7:
        ESP_LOGW(TAG, "st1615 error code: Invalid Secondary Touch Firmware!");
        break;
    default:
        ESP_LOGE(TAG, "st1615 status read error!");
        break;
    }

    if (status == ESP_OK)
    {
        return ESP_OK;
    }
    else
    {
        return ESP_FAIL;
    }
}

/**
 * @brief st1615获取手势
 *
 * @param num
 * @return gesture_num
 */
static gesture_num st1615_get_gesture_num(int8_t num)
{
    switch (num)
    {
    case 1:
#if ESP_DEBUG_LOGI
        ESP_LOGI(TAG, "gesture:Double_taps %d", num);
#endif
        return 1;
        break;
    case 2:
#if ESP_DEBUG_LOGI
        ESP_LOGI(TAG, "gesture:Zoom_in %d", num);
#endif
        return 2;
        break;
    case 3:
#if ESP_DEBUG_LOGI
        ESP_LOGI(TAG, "gesture:Zoom_out %d", num);
#endif
        return 3;
        break;
    case 4:
#if ESP_DEBUG_LOGI
        ESP_LOGI(TAG, "gesture:Left_to_right_slide %d", num);
#endif
        return 4;
        break;
    case 5:
#if ESP_DEBUG_LOGI
        ESP_LOGI(TAG, "gesture:Right_to_left_slide %d", num);
#endif
        return 5;
        break;
    case 6:
#if ESP_DEBUG_LOGI
        ESP_LOGI(TAG, "gesture:Top_to_down_slide %d", num);
#endif
        return 6;
        break;
    case 7:
#if ESP_DEBUG_LOGI
        ESP_LOGI(TAG, "gesture:Down_to_top_slide %d", num);
#endif
        return 7;
        break;
    case 8:
#if ESP_DEBUG_LOGI
        ESP_LOGI(TAG, "gesture:Palm %d", num);
#endif
        return 8;
        break;
    case 9:
#if ESP_DEBUG_LOGI
        ESP_LOGI(TAG, "gesture:Single_tap %d", num);
#endif
        return 9;
        break;
    default:
        return ESP_FAIL;
        break;
    }
    return ESP_FAIL;
}

/**
 * @brief st1615读取触屏坐标
 *
 * @param x
 * @param y
 * @return esp_err_t
 */
esp_err_t st1615_read_pos(uint16_t *x, uint16_t *y)
{
    esp_err_t ret = ESP_OK;

    uint8_t data[5];
    uint8_t data_x_h;
    uint8_t data_y_h;
    ret |= st1615_read_len(Advanced_Touch_Info, data, 5);

    /* x y high data */
    data_x_h = ((data[2] & 0x70) >> 4);
    data_y_h = (data[2] & 0x07);
    /* x y low data and high data */

    *x = (data_x_h << 8) | data[3];
    *y = (data_y_h << 8) | data[4];

#if ST1615_TEST
    st1615_get_gesture_num(data[0]);
    printf_bin(data[0]);
    printf_bin(data[1]);
#endif

    return ret;
}
