#pragma once

#include "esp_lcd_touch.h"

#ifdef __cplusplus
extern "C" {
#endif

void st1615_task(void *arg);

/**
 * @brief Create a new ST1615 touch driver
 *
 * @note The I2C communication should be initialized before use this function.
 *
 * @param io LCD/Touch panel IO handle
 * @param config: Touch configuration
 * @param out_touch: Touch instance handle
 * @return
 *      - ESP_OK                    on success
 *      - ESP_ERR_NO_MEM            if there is no memory for allocating main structure
 */
esp_err_t esp_lcd_touch_new_i2c_st1615(void);

esp_err_t st1615_read_pos(uint16_t *x, uint16_t *y);

/**
 * @brief esp lcd touch st1615 gesture num
 * 
 */
typedef enum esp_lcd_touch_st1615
{
    St1615_False = -1,
    No_gesture = 0,
    Double_taps =1,
    Zoom_in = 2,
    Zoom_out = 3,
    Left_to_right_slide = 4,
    Right_to_left_slide = 5,
    Top_to_down_slide = 6,
    Down_to_top_slide = 7,
    Palm = 8,
    Single_tap = 9,
    Long_press = 10,
} gesture_num;

/**
 * @brief I2C address of the ST1615 controller
 *
 */
#define ESP_LCD_TOUCH_IO_I2C_ST1615_ADDRESS (0x55)

/**
 * @brief Touch IO configuration structure
 *
 */
#define ESP_LCD_TOUCH_IO_I2C_ST1615_CONFIG()             \
    {                                                    \
        .dev_addr = ESP_LCD_TOUCH_IO_I2C_ST1615_ADDRESS, \
        .control_phase_bytes = 1,                        \
        .dc_bit_offset = 0,                              \
        .lcd_cmd_bits = 8,                               \
        .flags =                                         \
        {                                                \
            .disable_control_phase = 1,                  \
        }                                                \
    }

#ifdef __cplusplus
}
#endif