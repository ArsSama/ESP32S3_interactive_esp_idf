/********************************************************************************
 * @File name: lv_app_main.c
 * @Author: Letitia-Ron/PU-TONG/ArsSama
 * @Version: 1.1
 * @Date: 2023-4-15
 * @Description: lv_app_main
 ********************************************************************************/

#include <stdio.h>
#include "lv_app_main.h"

static const char *TAG = "lv_app_main";

lv_timer_t *updata_task1 = NULL;
uint8_t flag_debug = 0;
static int32_t set_brightness_last, set_brightness_last_now;

/*注意添加了自定义中文字体*/
void lv_ui_init(void)
{
    LV_FONT_DECLARE(myFont);
    // 创建字体样式
    static lv_style_t style_font;
    lv_style_init(&style_font);
    lv_style_set_text_font(&style_font, &myFont);                                  // 样式使用自定义字体
    lv_style_set_text_color(&style_font, lv_palette_main(LV_PALETTE_LIGHT_GREEN)); // 设置字体颜色

    lv_obj_t *panel1 = lv_obj_create(lv_scr_act());
    lv_obj_set_height(panel1, LV_SIZE_CONTENT);
    lv_obj_set_size(panel1, 320, 160);
    lv_obj_set_align(panel1, LV_ALIGN_TOP_MID);

    lv_obj_t *obj1 = lv_label_create(panel1);
    lv_obj_set_size(obj1, 250, 50);
    lv_label_set_text(obj1, "动画很疯狂\n但让我们也谈谈梦幻般的音乐选择\n狂战士的配乐总是给人留下深刻的印象");
    lv_obj_add_style(obj1, &style_font, LV_STATE_DEFAULT);
    lv_obj_set_style_text_color(obj1, lv_color_make(0xff, 0x00, 0x00), 0);

    /* set grid layout of the panel1 */
    static lv_coord_t grid_1_col_dsc[] = {LV_GRID_CONTENT, 5, LV_GRID_CONTENT, LV_GRID_FR(2), LV_GRID_FR(1), LV_GRID_FR(1), LV_GRID_TEMPLATE_LAST};
    static lv_coord_t grid_1_row_dsc[] = {LV_GRID_CONTENT, LV_GRID_CONTENT, 10, LV_GRID_CONTENT, LV_GRID_CONTENT, LV_GRID_TEMPLATE_LAST};
    lv_obj_set_grid_cell(panel1, LV_GRID_ALIGN_STRETCH, 0, 2, LV_GRID_ALIGN_CENTER, 0, 1);
    lv_obj_set_grid_dsc_array(panel1, grid_1_col_dsc, grid_1_row_dsc);

    lv_obj_set_grid_cell(obj1, LV_GRID_ALIGN_START, 2, 2, LV_GRID_ALIGN_CENTER, 0, 1);

    lv_obj_t *panel2 = lv_obj_create(lv_scr_act());
    lv_obj_set_align(panel2, LV_ALIGN_BOTTOM_MID);
    lv_obj_set_size(panel2, 320, 160);
    lv_obj_t *label = lv_label_create(panel2);
    lv_label_set_text(label, "脑子好痒，好像要长脑子惹");
    lv_obj_set_size(label, 250, 100);
    lv_obj_set_style_text_font(label, &myFont, 0);
    lv_obj_center(label);
}

#if LV_100ASK_2048_SIMPLE_TEST != 0

static void game_2048_event_cb(lv_event_t *e)
{
    lv_event_code_t code = lv_event_get_code(e);
    lv_obj_t *obj_2048 = lv_event_get_target(e);
    lv_obj_t *label = lv_event_get_user_data(e);

    if (code == LV_EVENT_VALUE_CHANGED)
    {
        if (lv_100ask_2048_get_best_tile(obj_2048) >= 2048)
            lv_label_set_text(label, "#00b329 YOU WIN! #");
        else if (lv_100ask_2048_get_status(obj_2048))
            lv_label_set_text(label, "#00b329 www.100ask.net: # #ff0000 GAME OVER! #");
        else
            lv_label_set_text_fmt(label, "SCORE: #ff00ff %d #", lv_100ask_2048_get_score(obj_2048));
    }
}
static void new_game_btn_event_handler(lv_event_t *e)
{
    lv_obj_t *obj_2048 = lv_event_get_user_data(e);

    lv_100ask_2048_set_new_game(obj_2048);
}
void lv_100ask_2048_simple(void)
{
    /*Create 2048 game*/
    lv_obj_t *obj_2048 = lv_100ask_2048_create(lv_scr_act());
#if LV_FONT_MONTSERRAT_40
    lv_obj_set_style_text_font(obj_2048, &lv_font_montserrat_40, 0);
#endif
    lv_obj_set_size(obj_2048, 240, 240);
    lv_obj_center(obj_2048);

    /*Information*/
    lv_obj_t *label = lv_label_create(lv_scr_act());
    lv_label_set_recolor(label, true);
    lv_label_set_text_fmt(label, "SCORE: #ff00ff %d #", lv_100ask_2048_get_score(obj_2048));
    lv_obj_align_to(label, obj_2048, LV_ALIGN_OUT_TOP_RIGHT, 0, -10);

    lv_obj_add_event_cb(obj_2048, game_2048_event_cb, LV_EVENT_ALL, label);

    /*New Game*/
    lv_obj_t *btn = lv_btn_create(lv_scr_act());
    lv_obj_align_to(btn, obj_2048, LV_ALIGN_OUT_TOP_LEFT, 0, -25);
    lv_obj_add_event_cb(btn, new_game_btn_event_handler, LV_EVENT_CLICKED, obj_2048);

    label = lv_label_create(btn);
    lv_label_set_text(label, "New Game");
    lv_obj_center(label);
}

#endif /*SIMPLE_TEST*/

/**
 * @brief increase_lvgl_tick 心跳
 *
 * @param arg
 */
static void increase_lvgl_tick(void *arg)
{
    /* Tell LVGL how many milliseconds has elapsed */
    lv_tick_inc(LVGL_TICK_PERIOD_MS);
}

/**
 * @brief updata_task1_cb 数据更新回调任务
 *
 * @param tmr
 */
void updata_task1_cb(lv_timer_t *tmr)
{
    // ESP_LOGI(TAG, "Enter lv_timer cb");

    /*更新背光显示数据*/ ////这里可以放在对应控件的回调事件里面好些
    // set_brightness_last_now = lv_slider_get_value(ui_BLightSlider);
    // if (set_brightness_last_now != set_brightness_last)
    // {
    //     set_brightness_last = set_brightness_last_now;
    //     ESP_LOGI(TAG, "%ld", set_brightness_last_now);
    //     lv_label_set_text_fmt(ui_BLBrightnessLabel, "%ld%%", set_brightness_last_now * 2);
    //     lcd_bl_set(lv_slider_get_value(ui_BLightSlider));
    // }

    flag_debug += 1;      /*计时flag*/
    if (flag_debug == 15) /*1.5 sec*/
    {
        flag_debug = 0;

        /*更新CPU 温度*/
        // xSemaphoreTake(cpu_temp_mutex, portMAX_DELAY); // 获取CPU温度的互斥锁 -- 这种是阻塞获取
        if (xSemaphoreTake(cpu_temp_mutex, 0) == pdTRUE) // 获取CPU温度的互斥锁 -- 这种是不阻塞 
        {
            /*访问共享资源-CPU温度*/
            ESP_LOGI(TAG, "ui_HomeTempLabel %.2f", cpu_tsens_value);
            lv_label_set_text_fmt(ui_HomeTempLabel, "%.2fC", cpu_tsens_value);
            lv_label_set_text_fmt(ui_SensorTempLabel, "%.2fC", cpu_tsens_value);
            xSemaphoreGive(cpu_temp_mutex); // 释放互斥锁
        }

        /*更新本地时间*/
        // xSemaphoreTake(local_time_mutex, portMAX_DELAY);   // 获取本地时间的互斥锁
        if (xSemaphoreTake(local_time_mutex, 0) == pdTRUE) // 获取本地时间的互斥锁
        {
            /*访问共享资源-本地时间*/
            if (local_time.local_hour < 12)
            {
                ESP_LOGI(TAG, "AM %d:%d:%d", local_time.local_hour, local_time.local_min, local_time.local_sec);
                if (local_time.local_hour < 10)
                {
                    if (local_time.local_min < 10)
                    {
                        lv_label_set_text_fmt(ui_HomeTimeLabel, "AM 0%d:0%d", local_time.local_hour, local_time.local_min);
                        lv_label_set_text_fmt(ui_SensorTimeLabel, "AM 0%d:0%d", local_time.local_hour, local_time.local_min);
                    }
                    else
                    {
                        lv_label_set_text_fmt(ui_HomeTimeLabel, "AM 0%d:%d", local_time.local_hour, local_time.local_min);
                        lv_label_set_text_fmt(ui_SensorTimeLabel, "AM 0%d:%d", local_time.local_hour, local_time.local_min);
                    }
                }
                else
                {
                    lv_label_set_text_fmt(ui_HomeTimeLabel, "AM %d:%d", local_time.local_hour, local_time.local_min);
                    lv_label_set_text_fmt(ui_SensorTimeLabel, "AM %d:%d", local_time.local_hour, local_time.local_min);
                }
            }
            else
            {
                ESP_LOGI(TAG, "PM %d:%d:%d", local_time.local_hour, local_time.local_min, local_time.local_sec);
                if ((local_time.local_hour - 12) < 10)
                {
                    if (local_time.local_min < 10)
                    {
                        lv_label_set_text_fmt(ui_HomeTimeLabel, "PM 0%d:0%d", local_time.local_hour - 12, local_time.local_min);
                        lv_label_set_text_fmt(ui_SensorTimeLabel, "PM 0%d:0%d", local_time.local_hour - 12, local_time.local_min);
                    }
                    else
                    {
                        lv_label_set_text_fmt(ui_HomeTimeLabel, "PM 0%d:%d", local_time.local_hour - 12, local_time.local_min);
                        lv_label_set_text_fmt(ui_SensorTimeLabel, "PM 0%d:%d", local_time.local_hour - 12, local_time.local_min);
                    }
                }
                else
                {
                    lv_label_set_text_fmt(ui_HomeTimeLabel, "PM %d:%d", local_time.local_hour - 12, local_time.local_min);
                    lv_label_set_text_fmt(ui_SensorTimeLabel, "PM %d:%d", local_time.local_hour - 12, local_time.local_min);
                }
            }
            xSemaphoreGive(local_time_mutex); // 释放互斥锁
        }

        /*更新传感器温湿度*/
        lv_label_set_text_fmt(ui_SenorDataLabel, "humi: %.2f %%\ntemp: %.2f C", post_data + 10, post_data); // 测试变量
    }
}

/**
 * @brief gui_task
 *
 * @param arg
 */
void gui_task(void *arg)
{
    /*lvgl内核初始化*/
    lv_init();
    /*屏幕初始化*/
    lv_port_disp_init();
    /*设备初始化接入*/
    // lv_port_indev_init();

    ESP_LOGI(TAG, "Install LVGL tick timer");
    // Tick interface for LVGL (using esp_timer to generate 2ms periodic event)
    const esp_timer_create_args_t lvgl_tick_timer_args = {
        .callback = &increase_lvgl_tick,
        .name = "lvgl_tick"};
    esp_timer_handle_t lvgl_tick_timer = NULL;
    ESP_ERROR_CHECK(esp_timer_create(&lvgl_tick_timer_args, &lvgl_tick_timer));
    ESP_ERROR_CHECK(esp_timer_start_periodic(lvgl_tick_timer, LVGL_TICK_PERIOD_MS * 1000));

    ESP_LOGI(TAG, "Start Run UI||Demo");

    /*test ui*/
    // lv_ui_init();

    /*my ui*/
    ui_init();
    // lv_100ask_pinyin_ime_simple_test();
    // lv_100ask_2048_simple();
    // lv_demo_music();
    // lv_demo_benchmark();

    /*lvgl定时任务*/
    updata_task1 = lv_timer_create(updata_task1_cb, 100, 0);
    // lv_timer_set_repeat_count(updata_task1,10);//如果不写就一直进入

    while (1)
    {
        // raise the task priority of LVGL and/or reduce the handler period can improve the performance
        vTaskDelay(pdMS_TO_TICKS(10));
        // The task running lv_timer_handler should have lower priority than that running `lv_tick_inc`
        lv_timer_handler();
    }
}