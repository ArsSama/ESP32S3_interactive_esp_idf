// This file was generated by SquareLine Studio
// SquareLine Studio version: SquareLine Studio 1.3.3
// LVGL version: 8.2.0
// Project name: SquareLine_Project

#include "../ui.h"

void ui_SensorScreen_screen_init(void)
{
ui_SensorScreen = lv_obj_create(NULL);
lv_obj_clear_flag( ui_SensorScreen, LV_OBJ_FLAG_SCROLLABLE );    /// Flags
lv_obj_set_style_bg_img_src( ui_SensorScreen, &ui_img_homebg_png, LV_PART_MAIN | LV_STATE_DEFAULT );

ui_SensorTopPanel = lv_obj_create(ui_SensorScreen);
lv_obj_set_width( ui_SensorTopPanel, 330);
lv_obj_set_height( ui_SensorTopPanel, 30);
lv_obj_set_x( ui_SensorTopPanel, 0 );
lv_obj_set_y( ui_SensorTopPanel, -5 );
lv_obj_set_align( ui_SensorTopPanel, LV_ALIGN_TOP_MID );
lv_obj_clear_flag( ui_SensorTopPanel, LV_OBJ_FLAG_SCROLLABLE );    /// Flags
lv_obj_set_style_radius(ui_SensorTopPanel, 0, LV_PART_MAIN| LV_STATE_DEFAULT);
lv_obj_set_style_bg_color(ui_SensorTopPanel, lv_color_hex(0x000000), LV_PART_MAIN | LV_STATE_DEFAULT );
lv_obj_set_style_bg_opa(ui_SensorTopPanel, 0, LV_PART_MAIN| LV_STATE_DEFAULT);
lv_obj_set_style_border_color(ui_SensorTopPanel, lv_color_hex(0xFFFFFF), LV_PART_MAIN | LV_STATE_DEFAULT );
lv_obj_set_style_border_opa(ui_SensorTopPanel, 255, LV_PART_MAIN| LV_STATE_DEFAULT);

ui_SensorTimeLabel = lv_label_create(ui_SensorTopPanel);
lv_obj_set_width( ui_SensorTimeLabel, LV_SIZE_CONTENT);  /// 1
lv_obj_set_height( ui_SensorTimeLabel, LV_SIZE_CONTENT);   /// 1
lv_obj_set_x( ui_SensorTimeLabel, -110 );
lv_obj_set_y( ui_SensorTimeLabel, 0 );
lv_obj_set_align( ui_SensorTimeLabel, LV_ALIGN_CENTER );
lv_label_set_text(ui_SensorTimeLabel,"AM 12:00");

ui_SensorTopLabel = lv_label_create(ui_SensorTopPanel);
lv_obj_set_width( ui_SensorTopLabel, LV_SIZE_CONTENT);  /// 1
lv_obj_set_height( ui_SensorTopLabel, LV_SIZE_CONTENT);   /// 1
lv_obj_set_align( ui_SensorTopLabel, LV_ALIGN_CENTER );
lv_label_set_text(ui_SensorTopLabel,"HOME");

ui_SensorTempLabel = lv_label_create(ui_SensorTopPanel);
lv_obj_set_width( ui_SensorTempLabel, LV_SIZE_CONTENT);  /// 1
lv_obj_set_height( ui_SensorTempLabel, LV_SIZE_CONTENT);   /// 1
lv_obj_set_x( ui_SensorTempLabel, 110 );
lv_obj_set_y( ui_SensorTempLabel, 0 );
lv_obj_set_align( ui_SensorTempLabel, LV_ALIGN_CENTER );
lv_label_set_text(ui_SensorTempLabel,"25.6C");

ui_SensorImgButtonPanel = lv_obj_create(ui_SensorScreen);
lv_obj_set_width( ui_SensorImgButtonPanel, 50);
lv_obj_set_height( ui_SensorImgButtonPanel, 40);
lv_obj_set_x( ui_SensorImgButtonPanel, 0 );
lv_obj_set_y( ui_SensorImgButtonPanel, -110 );
lv_obj_set_align( ui_SensorImgButtonPanel, LV_ALIGN_CENTER );
lv_obj_clear_flag( ui_SensorImgButtonPanel, LV_OBJ_FLAG_SCROLLABLE );    /// Flags
lv_obj_set_style_bg_color(ui_SensorImgButtonPanel, lv_color_hex(0xFFFFFF), LV_PART_MAIN | LV_STATE_DEFAULT );
lv_obj_set_style_bg_opa(ui_SensorImgButtonPanel, 0, LV_PART_MAIN| LV_STATE_DEFAULT);
lv_obj_set_style_border_color(ui_SensorImgButtonPanel, lv_color_hex(0x000000), LV_PART_MAIN | LV_STATE_DEFAULT );
lv_obj_set_style_border_opa(ui_SensorImgButtonPanel, 0, LV_PART_MAIN| LV_STATE_DEFAULT);

ui_SensorImgButton = lv_imgbtn_create(ui_SensorImgButtonPanel);
lv_imgbtn_set_src(ui_SensorImgButton, LV_IMGBTN_STATE_RELEASED, NULL, &ui_img_up_png, NULL);
lv_imgbtn_set_src(ui_SensorImgButton, LV_IMGBTN_STATE_PRESSED, NULL, &ui_img_frame_png, NULL);
lv_obj_set_width( ui_SensorImgButton, 16);
lv_obj_set_height( ui_SensorImgButton, 16);
lv_obj_set_align( ui_SensorImgButton, LV_ALIGN_CENTER );

ui_SensorDataBootPanel = lv_obj_create(ui_SensorScreen);
lv_obj_set_width( ui_SensorDataBootPanel, 225);
lv_obj_set_height( ui_SensorDataBootPanel, 160);
lv_obj_set_x( ui_SensorDataBootPanel, 0 );
lv_obj_set_y( ui_SensorDataBootPanel, 40 );
lv_obj_set_align( ui_SensorDataBootPanel, LV_ALIGN_CENTER );
lv_obj_clear_flag( ui_SensorDataBootPanel, LV_OBJ_FLAG_SCROLLABLE );    /// Flags
lv_obj_set_style_radius(ui_SensorDataBootPanel, 0, LV_PART_MAIN| LV_STATE_DEFAULT);
lv_obj_set_style_bg_color(ui_SensorDataBootPanel, lv_color_hex(0xFF0000), LV_PART_MAIN | LV_STATE_DEFAULT );
lv_obj_set_style_bg_opa(ui_SensorDataBootPanel, 50, LV_PART_MAIN| LV_STATE_DEFAULT);
lv_obj_set_style_border_color(ui_SensorDataBootPanel, lv_color_hex(0xFFFFFF), LV_PART_MAIN | LV_STATE_DEFAULT );
lv_obj_set_style_border_opa(ui_SensorDataBootPanel, 255, LV_PART_MAIN| LV_STATE_DEFAULT);

ui_SensorDataBootPanelLabel = lv_label_create(ui_SensorDataBootPanel);
lv_obj_set_width( ui_SensorDataBootPanelLabel, LV_SIZE_CONTENT);  /// 1
lv_obj_set_height( ui_SensorDataBootPanelLabel, LV_SIZE_CONTENT);   /// 1
lv_obj_set_x( ui_SensorDataBootPanelLabel, 0 );
lv_obj_set_y( ui_SensorDataBootPanelLabel, -5 );
lv_obj_set_align( ui_SensorDataBootPanelLabel, LV_ALIGN_TOP_MID );
lv_label_set_text(ui_SensorDataBootPanelLabel,"Data \nacquisition");
lv_obj_set_style_text_align(ui_SensorDataBootPanelLabel, LV_TEXT_ALIGN_CENTER, LV_PART_MAIN| LV_STATE_DEFAULT);
lv_obj_set_style_text_font(ui_SensorDataBootPanelLabel, &lv_font_montserrat_18, LV_PART_MAIN| LV_STATE_DEFAULT);

ui_SensorDatamainPanel = lv_obj_create(ui_SensorDataBootPanel);
lv_obj_set_width( ui_SensorDatamainPanel, 225);
lv_obj_set_height( ui_SensorDatamainPanel, 120);
lv_obj_set_x( ui_SensorDatamainPanel, 0 );
lv_obj_set_y( ui_SensorDatamainPanel, 40 );
lv_obj_set_align( ui_SensorDatamainPanel, LV_ALIGN_CENTER );
lv_obj_clear_flag( ui_SensorDatamainPanel, LV_OBJ_FLAG_SCROLLABLE );    /// Flags
lv_obj_set_style_radius(ui_SensorDatamainPanel, 0, LV_PART_MAIN| LV_STATE_DEFAULT);
lv_obj_set_style_bg_color(ui_SensorDatamainPanel, lv_color_hex(0xFFFFFF), LV_PART_MAIN | LV_STATE_DEFAULT );
lv_obj_set_style_bg_opa(ui_SensorDatamainPanel, 50, LV_PART_MAIN| LV_STATE_DEFAULT);
lv_obj_set_style_border_color(ui_SensorDatamainPanel, lv_color_hex(0xFFFFFF), LV_PART_MAIN | LV_STATE_DEFAULT );
lv_obj_set_style_border_opa(ui_SensorDatamainPanel, 255, LV_PART_MAIN| LV_STATE_DEFAULT);

ui_SensorHumiImage = lv_img_create(ui_SensorDatamainPanel);
lv_img_set_src(ui_SensorHumiImage, &ui_img_humidity_png);
lv_obj_set_width( ui_SensorHumiImage, LV_SIZE_CONTENT);  /// 1
lv_obj_set_height( ui_SensorHumiImage, LV_SIZE_CONTENT);   /// 1
lv_obj_set_x( ui_SensorHumiImage, -86 );
lv_obj_set_y( ui_SensorHumiImage, -30 );
lv_obj_set_align( ui_SensorHumiImage, LV_ALIGN_CENTER );
lv_obj_add_flag( ui_SensorHumiImage, LV_OBJ_FLAG_ADV_HITTEST );   /// Flags
lv_obj_clear_flag( ui_SensorHumiImage, LV_OBJ_FLAG_SCROLLABLE );    /// Flags

ui_SensorTempImage = lv_img_create(ui_SensorDatamainPanel);
lv_img_set_src(ui_SensorTempImage, &ui_img_temperature_png);
lv_obj_set_width( ui_SensorTempImage, LV_SIZE_CONTENT);  /// 1
lv_obj_set_height( ui_SensorTempImage, LV_SIZE_CONTENT);   /// 1
lv_obj_set_x( ui_SensorTempImage, -86 );
lv_obj_set_y( ui_SensorTempImage, 12 );
lv_obj_set_align( ui_SensorTempImage, LV_ALIGN_CENTER );
lv_obj_add_flag( ui_SensorTempImage, LV_OBJ_FLAG_ADV_HITTEST );   /// Flags
lv_obj_clear_flag( ui_SensorTempImage, LV_OBJ_FLAG_SCROLLABLE );    /// Flags

ui_SenorDataLabel = lv_label_create(ui_SensorDatamainPanel);
lv_obj_set_width( ui_SenorDataLabel, LV_SIZE_CONTENT);  /// 1
lv_obj_set_height( ui_SenorDataLabel, LV_SIZE_CONTENT);   /// 1
lv_obj_set_x( ui_SenorDataLabel, -2 );
lv_obj_set_y( ui_SenorDataLabel, -10 );
lv_obj_set_align( ui_SenorDataLabel, LV_ALIGN_CENTER );
lv_label_set_text(ui_SenorDataLabel,": humi: 86.56 %\n\n: temp: 26.53 C");
lv_obj_set_style_text_font(ui_SenorDataLabel, &lv_font_montserrat_18, LV_PART_MAIN| LV_STATE_DEFAULT);

lv_obj_add_event_cb(ui_SensorImgButton, ui_event_SensorImgButton, LV_EVENT_ALL, NULL);
lv_obj_add_event_cb(ui_SensorImgButtonPanel, ui_event_SensorImgButtonPanel, LV_EVENT_ALL, NULL);
lv_obj_add_event_cb(ui_SensorScreen, ui_event_SensorScreen, LV_EVENT_ALL, NULL);

}
