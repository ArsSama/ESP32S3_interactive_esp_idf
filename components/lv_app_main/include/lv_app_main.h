#ifndef __LV_APP_MAIN_H
#define __LV_APP_MAIN_H

#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_timer.h"
#include "esp_lcd_panel_io.h"
#include "esp_lcd_panel_vendor.h"
#include "esp_lcd_panel_ops.h"
#include "driver/gpio.h"
#include "esp_err.h"
#include "esp_log.h"

/*LVGL*/
#include "lvgl.h"
/*I80_BSP*/
#include "i80.h"
/*LVGL_Display_Port*/
#include "lvgl_port.h"
/*my ui*/
#include "../ui.h"
#include "../ui_helpers.h"
#include "demos/lv_demos.h"
/*100 ask*/
#include "lv_lib_100ask.h"
/*cpu temperature*/
#include "cpu_temperature_sensor.h"
/*ntp_local_time*/
#include "sntp_custom.h"
/*sensor himi tenp*/
#include "http_post.h"
/*mutex*/
#include "mutex.h"

#define LVGL_TICK_PERIOD_MS 2

void gui_task(void *arg);

#endif
